package logic;

public class NotWff extends Wff {
	
	private Wff left;
	private Operator op;
	
	public NotWff(Wff left) {
		this.left = left;
		this.op = Operator.NOT;
	}
	
	public boolean eval(Valuation val) {
		return !this.left.eval(val);
	}
	
	public String toString() {
		return "~" + this.left.toString();
	}
	
	public Wff getLeft() {
		return this.left;
	}
	
	public Operator getOp(){
		return this.op;
	}

}
