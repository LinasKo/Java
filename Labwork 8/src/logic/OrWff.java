package logic;

public class OrWff extends Wff {
	
	private Wff left, right;
	private Operator op;
	
	public OrWff(Wff left, Wff right) {
		this.left = left;
		this.right = right;
		this.op = Operator.OR;
	}
	
	public boolean eval(Valuation val) {
		return this.left.eval(val) || this.right.eval(val);
	}
	
	public String toString() {
		return "(" + this.left.toString() + " | " + this.right.toString() + ")";
	}
	
	public Wff getLeft() {
		return this.left;
	}
	
	public Wff getRight() {
		return this.right;
	}
	
	public Operator getOp(){
		return this.op;
	}

}
