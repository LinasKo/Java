package logic;

public abstract class BinaryWff extends Wff {
	
	public Wff left, right;
	public Operator op;
	
	public BinaryWff(){};
	
	public BinaryWff(Wff left, Wff right) {
		this.left = left;
		this.right = right;
	}
	
	public void setOp(Operator op) {
		this.op = op;
	}
	
	public Operator getOp() {
		return this.op;
	}
	
	public Wff getLeft() {
		return left;
	}

	public Wff getRight() {
		return right;
	}
	
	public String toString() {
		return "(" + this.left + " " + this.op + " " + this.right + ")";
	}
}