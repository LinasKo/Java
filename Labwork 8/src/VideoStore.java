import java.util.ArrayList;

public class VideoStore {
	
	private ArrayList<Video> inventory = new ArrayList<Video>();
	
	public boolean addVideo(String title) {
		for (Video s : this.inventory) {
			if (s.getTitle() == title) {
				System.out.print(title + " is already in stock.\n");
				return false;
			}
		}
		this.inventory.add(new Video(title));
		return true;
	}
	
	public Video getVideo(String title) {
		for (Video s : this.inventory) {
			if (s.getTitle() == title) {
				return s;
			}
		}
		System.out.print("Sorry, cannot find the requested video in the catalogue\n");
		return null;
	}
	
	public boolean checkOutVideo(String title) {
		boolean in = false;
		for (Video v : this.inventory) {
			if (v.getTitle() == title) {
				in = true;
			}
		}
		if (in) {
			return getVideo(title).checkOut();
		}
		else {
			System.out.print("Sorry, cannot find the requested video in the catalogue\n");
			return false;
		}
	}
	
	public boolean returnVideo(Video video) {
		boolean in = false;
		for (Video v : this.inventory) {
			if (v == video) {
				in = true;
			}
		}
		if (in) {
			return video.returnToStore();
		}
		else {
			System.out.print("Sorry, this video did not come from this store\n");
		}
		return false;
	}
	
	public void rateVideo(Video video, int rating) {
		video.addRating(rating);
	}
	
	public double getAverageRatingForVideo(Video video) {
		return video.getAverageRating();
	}
	
	public Video[] getCheckedOut() {
		ArrayList<Video> res = new ArrayList<Video>();
		for (Video v : this.inventory) {
			if (v.isCheckedOut()) {
				res.add(v);
			}
		}
		return res.toArray(new Video[res.size()]);
	}
	
	public Video mostPopular() {
		double max = 0.0;
		Video vid = null;
		for (Video v : this.inventory) {
			if (this.getAverageRatingForVideo(v) > max) {
				max = this.getAverageRatingForVideo(v);
				vid = v;
			}
		}
		return vid;
	}
	
	
	
}
