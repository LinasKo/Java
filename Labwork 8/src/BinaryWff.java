
public class BinaryWff {
	
	public PropVar left, right;
	public Operator op;
	
	public BinaryWff(){};
	
	public BinaryWff(PropVar left, PropVar right) {
		this.left = left;
		this.right = right;
	}
	
	public void setOp(Operator op) {
		this.op = op;
	}
	
	public Operator getOp() {
		return this.op;
	}
	
	public PropVar getLeft() {
		return left;
	}

	public PropVar getRight() {
		return right;
	}
	
	public String toString() {
		return "(" + this.left + " " + this.op + " " + this.right + ")";
	}
}
