
public class AndWff extends BinaryWff {
	
	public AndWff(PropVar left, PropVar right) {
		this.op = Operator.AND;
		this.left = left;
		this.right = right;
	}	
}
