
public class IfWff extends BinaryWff {
	public IfWff(PropVar left, PropVar right) {
		this.op = Operator.IF;
		this.left = left;
		this.right = right;
	}	
}
