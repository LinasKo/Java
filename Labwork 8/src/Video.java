
import java.util.ArrayList;

public class Video {

	private String title;
	private boolean flag = false;
	private ArrayList<Integer> ratings = new ArrayList<Integer>();
	
	public Video(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}
	
	public boolean addRating(int rating) {
		if (rating >= 1 && rating <= 5) {
			this.ratings.add(rating);
			return true;
		}
		else {
			System.out.print(rating + " should be between 1 and 5.\n");
			return false;
		}
	}
	
	public double getAverageRating() {
		if (this.ratings.size() == 0) {
			return 0.0;
		}
		int sum = 0;
		for (int r : this.ratings) {
			sum += r;
		}
		return (double) sum/this.ratings.size();
	}
	
	public boolean checkOut() {
		if (this.flag == true) { // is checked out.
			System.out.print(this.toString() + " is already checked out.\n");
			return false;
		}
		else {
			this.flag = true;
			return true;
		}
	}
	
	public boolean returnToStore() {
		if (this.flag == false) { // is not checked out
			System.out.print(this.toString() + " is not checked out.\n");
			return false;
		}
		else {
			this.flag = false;
			return true;
		}
	}
	
	public boolean isCheckedOut() {
		return this.flag;
	}
	
	public String toString() {
		return "Video[title=\"" + this.title + "\", checkedOut=" + this.isCheckedOut() + "]";
	}
}


