public class OneB {
	static double meanColSums(int[][] matrix) {
		double[] sums = new double[matrix[0].length];
		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[0].length; col++) {
				sums[col] += matrix[row][col];
			}
		}

		double res = 0;
		for (double sum : sums)
			res += sum;
		res /= sums.length;

		return res;
	}
}
