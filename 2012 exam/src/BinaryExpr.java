public class BinaryExpr extends Expr {

	private Expr l, r;
	private Op op;

	BinaryExpr(Expr l, Op op, Expr r) {
		this.l = l;
		this.op = op;
		this.r = r;
	}

	@Override
	boolean isTerm() {
		return (l.isTerm() && op == Op.PRODUCT && r.isTerm());
	}

	@Override
	boolean isNorm() {
		return ((l.isNorm() && op == Op.SUM && r.isNorm()) || this.isTerm());
	}

	public Expr getLeft() {
		return l;
	}

	public Expr getRight() {
		return r;
	}

	public Op getOp() {
		return op;
	}

	public String toString() {
		return "(" + l.toString() + " " + op.toString() + " " + r.toString()
				+ ")";
	}

	Expr normalize() {
		if (this.isNorm())
			return this;
		else {
			if (l.getOp() == Op.SUM) {
				BinaryExpr nleft = new BinaryExpr(l.getLeft(), Op.PRODUCT, r);
				BinaryExpr nright = new BinaryExpr(l.getRight(), Op.PRODUCT, r);
				return new BinaryExpr(nleft.normalize(), Op.SUM,
						nright.normalize());
			} else {
				BinaryExpr nleft = new BinaryExpr(l, Op.PRODUCT, r.getLeft());
				BinaryExpr nright = new BinaryExpr(l, Op.PRODUCT, r.getRight());
				return new BinaryExpr(nleft.normalize(), Op.SUM,
						nright.normalize());
			}
		}
	}
}
