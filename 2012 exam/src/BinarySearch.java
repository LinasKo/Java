
public class BinarySearch {
	
	public static void main(String[] args) {
		int[] arr = new int[]{12,45,78,89,104};
		System.out.println(search(104, arr, 0, arr.length));
	}
	
	public static int search(int target, int[] nums, int leftIndex, int rightIndex) {
		if (leftIndex == rightIndex) return -1;
		int i_half = (rightIndex + leftIndex) / 2;
		if (nums[i_half] == target)
			return i_half;
		else if (target < nums[i_half]) {
			return search(target, nums, leftIndex, i_half);
		}
		else {
			return search(target, nums, i_half+1, rightIndex);
		}
	}
}
