public class OneA {
	
	static int prodOfPairs(int[] nums) {
		if (nums.length % 2 == 1) return -1;
		else {
			int res = 0;
			for (int i = 0; i < nums.length/2; i++) {
				res += nums[i*2]*nums[i*2+1];
			}
			return res;
		}
	}
	
}
