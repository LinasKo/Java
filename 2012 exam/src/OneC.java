import java.util.ArrayList;

public class OneC {
	static int hammingDist(String left, String right) {
		int dist = 0;
		for (int a = 0; a < left.length(); a++) {
			if (left.charAt(a) != right.charAt(a)) {
				dist++;
			}
		}
		return dist;
	}

	static String findFarthest(String s, String[] targets) {

		String farthest = s;
		int longest_dist = 0;
		for (String t : targets) {
			if (hammingDist(s, t) > longest_dist) {
				longest_dist = hammingDist(s, t);
				farthest = t;
			}
		}
		return farthest;
	}

	static ArrayList<String> findNearestK(String s, String[] targets, int k) {

		ArrayList<String> res = new ArrayList<String>();
		res.add(s);
		for (String t : targets) {
			if (hammingDist(s, t) <= k)
				res.add(t);
		}
		return res;
	}

	static int stringDist(String left, String right) {
		String a, b;
		if (right.length() > left.length()) {
			a = left;
			b = right;
		} else {
			a = right;
			b = left;
		}
		
		int n = b.length() - a.length();
		b = b.substring(0, b.length()-n);
		
		return hammingDist(a, b) + n;
	}
}
