public class Var extends Expr {

	private String symbol;

	Var(String symbol) {
		this.symbol = symbol;
	}

	@Override
	boolean isTerm() {
		return true;
	}

	@Override
	boolean isNorm() {
		return true;
	}
	
	public String toString(){
		return symbol;
	}

}
