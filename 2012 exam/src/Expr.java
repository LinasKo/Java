public abstract class Expr {
	abstract boolean isTerm();

	abstract boolean isNorm();

	Expr normalize() {
		return this;
	}
	
	public Expr getRight() {
		return null; 
	}
	
	public Expr getLeft() {
		return null; 
	}
	
	public Op getOp() {
		return null; 
	}
}
