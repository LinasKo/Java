public class ExprClient {

	// isTerm, isNorm, normalize

	public static void main(String[] args) {

		Var x = new Var("x");
		Var y = new Var("y");
		Var z = new Var("z");

		BinaryExpr e0 = new BinaryExpr(x, Op.PRODUCT, y);
		BinaryExpr e1 = new BinaryExpr(x, Op.SUM, y);
		BinaryExpr e2 = new BinaryExpr(e0, Op.SUM, z);
		BinaryExpr e3 = new BinaryExpr(e1, Op.PRODUCT, z);
		BinaryExpr e4 = new BinaryExpr(e0, Op.PRODUCT, z);
		BinaryExpr e5 = new BinaryExpr(e1, Op.PRODUCT, e1);

		System.out.println("-- isTerm --");
		System.out.println(x.isTerm());
		System.out.println(e0.isTerm());
		System.out.println(e1.isTerm());
		System.out.println(e2.isTerm());
		System.out.println(e3.isTerm());
		System.out.println(e4.isTerm());
		System.out.println(e5.isTerm());

		System.out.println("-- isNorm --");
		System.out.println(x.isNorm());
		System.out.println(e0.isNorm());
		System.out.println(e1.isNorm());
		System.out.println(e2.isNorm());
		System.out.println(e3.isNorm());
		System.out.println(e4.isNorm());
		System.out.println(e5.isNorm());

		System.out.println("-- normalize --");
		System.out.println(x.normalize());
		System.out.println(e0.normalize());
		System.out.println(e3.normalize());
		System.out.println(e5.normalize());
	}
}
