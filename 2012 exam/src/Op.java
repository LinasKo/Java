
public enum Op {
	PRODUCT, SUM;
	
	public String toString() {
		if (this == PRODUCT) return "*";
		else if (this == SUM) return "+";
		else return null;
	}
}
