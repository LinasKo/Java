package main_package;
public class OneA {

    public static double geometricMean(int[] nums) {
    	double res = 1;
        for (int i: nums) {
        	res *= Math.pow(i, 1.0/nums.length);
        }
        return res;
    }
}
