package main_package;
public class Element {

	private String element, sym;
	private int atomicNum;
	private double weight;
	
	Element(String element, int atomicNum, String sym, double weight) {
		this.element = element;
		this.atomicNum = atomicNum;
		this.sym = sym;
		this.weight = weight;
	}

	public String getElement() {
		return element;
	}

	public String getSym() {
		return sym;
	}

	public int getAtomicNum() {
		return atomicNum;
	}

	public double getWeight() {
		return weight;
	}
	public String toString(){
		return "Element(" + element+ ", " +atomicNum + ", " + sym + ", " + weight +")";
	}
	

}
