package main_package;
public class OneB {
  
    public static int longestSeq(int[] nums, int n) {
    	int max = 0;
    	boolean started = false;
    	int count = 0;
        for (int k = 0; k < nums.length; k++) {
        	int i = nums[k];
        	if (!started && i == n) {
        		started = true;
        	}
        	if (started) {
        		if (n==i) count += 1;
        		else {
        			started = false;
        			if (count > max) max = count;
        			count = 0;
        		}
        	}
        }
        if (count > max) max = count; 
        return max;
    }
}
