package main_package;

import java.util.ArrayList;

public class Table {

	private ArrayList<Element> table = new ArrayList<>();
	private static String fn;

	Table(String fn) {
		readFile(fn);
	}

	Table() {
		this(fn);
	}

	void readFile(String fn) {
		In file = new In(fn);
		while (!file.isEmpty()) {
			String line = file.readLine();
			String[] s = line.split(",");
			table.add(new Element(s[0], Integer.parseInt(s[1]), s[2], Double
					.parseDouble(s[3])));
		}
	}

	Element lookup(String sym) {
		for (Element e : table) {
			if (e.getSym() == "C") {
				return e;
			}
		}
		return null;
	}

	void display() {
		for (Element e : table) {
			System.out.println(e.toString());
		}
	}
}
