package main_package;

import java.util.ArrayList;
import java.util.HashMap;

public class Molecule {

	private HashMap<Element, Integer> structure = new HashMap<>();
	private Table table = new Table();
	
	void addAtom(String sym, int num) {
		structure.put(table.lookup(sym), num);
	}
	
	void addAtom(String sym) {
		addAtom(sym, 1);
	}
	
	public ArrayList<Element> atoms(){
		return new ArrayList<Element>(structure.keySet());
	}
	
	double weight(){
		double sum = 0;
		for(Element e : atoms()) {
			sum += e.getWeight() * structure.get(e);
		}
		return sum;
	}
}
