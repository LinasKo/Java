package main_package;

import java.util.ArrayList;
import java.util.HashMap;

public class tester {

	public static void p(ArrayList<String> st) {
		for (String s : st) {
			System.out.print(s + ", ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		// OneA
		System.out.println(OneA.geometricMean(new int[] { 1, 2, 3 }));
		System.out.println(OneA.geometricMean(new int[] { 2, 8 }));
		System.out.println(OneA.geometricMean(new int[] { 3, 5, 7, 9 }));

		// OneB
		System.out.println(OneB.longestSeq(new int[] { 1, 1, 0, 0, 1, 0, 1, 1,
				1 }, 0));
		System.out.println(OneB.longestSeq(new int[] { 1, 1, 0, 0, 1, 0, 1, 1,
				1 }, 1));
		System.out.println(OneB.longestSeq(new int[] { 1, 1, 1, 1 }, 0));
		System.out.println(OneB.longestSeq(new int[] {}, 1));

		// OneC
		ArrayList<String> x1 = OneC.findSubstrings("abcc", 2);
		ArrayList<String> x2 = OneC.findSubstrings("abcd", 4);
		ArrayList<String> x3 = OneC.findSubstrings("abc", 4);
		ArrayList<String> x4 = OneC.findSubstrings("aa", 1);
		p(x1);
		p(x2);
		p(x3);
		p(x4);
		System.out.println();
		
		HashMap<String, Integer> freq = new HashMap<String, Integer>();
		freq.put("a", 1);
		OneC.increment(freq, "a");
		OneC.increment(freq, "b");
		System.out.println("a = " + freq.get("a"));
		System.out.println("b = " + freq.get("b"));
		System.out.println();
		
		HashMap<String, Integer> f1 = OneC.findStringCounts("abcdabcd");
		System.out.println(f1.get("abc") + ", " + f1.get("dab") + ", " + f1.get("bcd") + ", " + f1.get("cda"));
		HashMap<String, Integer> f2 = OneC.findStringCounts("abc");
		System.out.println(f2.get("abc"));
		HashMap<String, Integer> f3 = OneC.findStringCounts("ab");
		System.out.println(f3.get("ab"));
		HashMap<String, Integer> f4 = OneC.findStringCounts("XXXX");
		System.out.println(f4.get("XXX"));
	}
}
