package main_package;

import java.util.ArrayList;
import java.util.HashMap;

public class OneC {

	public static ArrayList<String> findSubstrings(String s, int len) {
		ArrayList<String> res = new ArrayList<>();
		if (len > s.length())
			return res;
		else {
			for (int k = 0; k <= s.length() - len; k++) {
				res.add(s.substring(k, k + len));
			}
		}
		return res;
	}

	public static void increment(HashMap<String, Integer> map, String s) {
		if (!map.containsKey(s))
			map.put(s, 1);
		else {
			map.put(s, map.get(s) + 1);
		}
	}

	public static HashMap<String, Integer> findStringCounts(String s) {
		HashMap<String, Integer> freq = new HashMap<String, Integer>();
		ArrayList<String> arr = findSubstrings(s, 3);
		for (String x : arr) {
			increment(freq, x);
		}
		return freq;
	}

}
