
public class MeanVariance {
	public static void main(String[] args) {
		int count = 0;
		double mean = 0;
		for (int i = 0; i < args.length; i++) {
			mean += Double.parseDouble(args[i]);
			count += 1;
		}
		mean = mean / count;
		
		double var = 0;
		for (int i = 0; i < args.length; i++) {
			var += Math.pow(Double.parseDouble(args[i]) - mean, 2);
		}
		var = var / count;
		
		System.out.println(mean);
		System.out.println(var);
	}
}
