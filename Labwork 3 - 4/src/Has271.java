
public class Has271 {

	public static boolean has271(int[] nums) {
		int[] data = {2,7,1};
		for (int i = 0; i < nums.length - 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (nums[i+j] == data[j]) {
					if (j == 2) {
						return true;
					}
				}
				else break;
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		int[] nums = new int[args.length];
		System.out.println(args.length);
		for (int i = 0; i < args.length; i++) {
			nums[i] = Integer.parseInt(args[i]);
		System.out.println(has271(nums));
		}
	}
}
