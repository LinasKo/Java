
public class ArrayRotate {
    public static void main(String args[]) {
        String[] nums = new String[args.length];
	for (int i = 0; i < args.length; i++) {
	    nums[i] = args[i];
	}

	String[] copy = new String[nums.length];
	for (int i = 0; i < nums.length; i++) {
	    copy[i] = nums[(i+1)%nums.length];
	}

	for (int i = 0; i < copy.length; i++) {
	    System.out.print(copy[i] + " ");
	}
	System.out.println();
    }
}
