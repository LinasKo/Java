
public class Sieve {

	public static void main(String[] args) {
		int[] primes = new int[19];
		for (int i = 0; i < 19; i++) {
			primes[i] = i+2;
		}
		for (int i = 0; i < 19; i++) {
			if (primes[i] != 0) {
				System.out.print(primes[i] + " ");
				for (int j = i+1; j < 19; j++) {
					if (primes[j] % primes[i] == 0) {
						primes[j] = 0;
					}
				}
			}
		}
	}
}
