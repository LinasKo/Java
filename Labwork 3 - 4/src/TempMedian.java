
import java.util.Arrays;

public class TempMedian {
	public static void main(String[] args) {
		int[] temps = new int[args.length];
		temps[0] = Integer.parseInt(args[0]);
		int curr = temps[0];
		for (int i = 1; i < args.length; i++){
			if (args[i].charAt(0) == '.'){
				temps[i] = curr;
			}
			else if (args[i].charAt(0) == '+') {
				curr += 1;
				temps[i] = curr;
			}
			else if (args[i].charAt(0) == '-') {
				curr -= 1;
				temps[i] = curr;
			}
		}
		for (int i = 0; i < (args.length-1); i++){
			System.out.print(temps[i] + " ");
		}
		System.out.print(temps[args.length-1]);
		System.out.print("\n");
		
		Arrays.sort(temps);
		for (int i = 0; i < (args.length-1); i++){
			System.out.print(temps[i] + " ");
		}
		System.out.print(temps[args.length-1]);
		System.out.print("\n");
		
		if (temps.length % 2 == 0){
			System.out.println((double)(temps[temps.length/2] + temps[temps.length/2-1])/2);
		}
		else System.out.println((double)temps[temps.length/2]);
	}
}
