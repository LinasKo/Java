
public class QuestionOneTester {

	public static void main(String[] args) {
		System.out.println(OneA.prodOfPairs(new int[] {1, 2, 3, 4}));
		System.out.println(OneA.prodOfPairs(new int[] {3, 5, 7, 5, -2, 4}));
		System.out.println(OneA.prodOfPairs(new int[] {}));
		System.out.println(OneA.prodOfPairs(new int[] {1, 2, 3}));
		System.out.println();
		
		System.out.println(OneB.meanColSums(new int[][] {{12, 1}, {2, 5}, {8, 1}}));
		System.out.println(OneB.meanColSums(new int[][] {{12, 1, 14}, {2, 5, 5}, {8, 1, 2}}));
		System.out.println(OneB.meanColSums(new int[][] {{1, 0, 0, 0, 0}}));
		System.out.println(OneB.meanColSums(new int[][] {{0}}));
		System.out.println();
		
		System.out.println(OneC.hammingDist("abaca", "abaca"));
		System.out.println(OneC.hammingDist("abaca", "aback"));
		System.out.println(OneC.hammingDist("abaca", "abaft"));
		System.out.println(OneC.hammingDist("abaca", "adapt"));
		System.out.println(OneC.hammingDist("abaca", "accoy"));
		System.out.println(OneC.hammingDist("abaca", "actor"));
		System.out.println();
		
		String[] targets = new String[]{"abaca", "aback", "abaft", "adapt",
				"accoy", "actor"};
		System.out.println(OneC.findFarthest("abaca", targets));
		System.out.println();
		
		for (String s :OneC.findNearestK("abaca", targets, 2)) {
			System.out.print(s + ",");
		}
		System.out.println();
		System.out.println();
		
		System.out.println(OneC.stringDist("heat", "heater"));
		System.out.println(OneC.stringDist("heater", "heat"));
		System.out.println(OneC.stringDist("heat", "hatter"));
		System.out.println();
	}

}
