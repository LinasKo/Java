
public class OneB {
	public static double meanColSums(int[][] matrix) {
		double res = 0;
		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[0].length; col++) {
				res += matrix[row][col];
			}
		}
		return res/matrix[0].length;
	}
}
