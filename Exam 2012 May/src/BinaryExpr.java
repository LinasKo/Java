
public class BinaryExpr extends Expr {
	private Expr l, r;
	private Op op;
	
	public BinaryExpr(Expr l, Op op, Expr r) {
		this.l = l;
		this.op = op;
		this.r = r;
	}

	@Override
	public boolean isTerm() {
		return l.isTerm() && op == Op.PRODUCT && r.isTerm();
	}

	@Override
	public boolean isNorm() {
		return this.isTerm() || (l.isNorm() && op == Op.SUM && r.isNorm());
	}

	public Expr getLeft() {
		return l;
	}

	public Expr getRight() {
		return r;
	}

	public Op getOp() {
		return op;
	}
	
	public String toString() {
		//return "(" + l.toString() + " " + op.toString() + " " + r.toString() + ")";
		return String.format("(%s %s %s)", l, op, r);
	}
	
	public Expr normalize() {
		if (this.isNorm()) return this;
		if (l.getOp() == Op.SUM) {
			BinaryExpr left = new BinaryExpr(l.getLeft(), Op.PRODUCT, r);
			BinaryExpr right = new BinaryExpr(l.getRight(), Op.PRODUCT, r);
			return new BinaryExpr(left.normalize(), Op.SUM, right.normalize());
		}
		else if (r.getOp() == Op.SUM){
			BinaryExpr left = new BinaryExpr(l, Op.PRODUCT, r.getLeft());
			BinaryExpr right = new BinaryExpr(l, Op.PRODUCT, r.getRight());
			return new BinaryExpr(left.normalize(), Op.SUM, right.normalize());
		}
		else return new BinaryExpr(l.normalize(), op, r.normalize());
		
	}
}
