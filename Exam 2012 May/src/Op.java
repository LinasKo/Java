
public enum Op {
	PRODUCT, SUM;
	
	public String toString(){
		if (this == PRODUCT) return "*";
		else return "+";
	}
}
