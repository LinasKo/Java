
public class OneA {
	public static int prodOfPairs(int[] nums){
		if (nums.length % 2 == 1) return -1;
		int sum = 0;
		for (int i = 0; i < nums.length; i+=2) {
			sum += nums[i]*nums[i+1];
		}
		return sum;
	}
}
