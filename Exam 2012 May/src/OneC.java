import java.util.ArrayList;


public class OneC {
	public static int hammingDist(String left, String right) {
		int dist = 0;
		for (int i = 0; i < left.length(); i++) {
			if (left.charAt(i) != right.charAt(i))
				dist++;
		}
		return dist;
	}
	
	public static String findFarthest(String s, String[] targets) {
		String max_s = "";
		int max_d = -1;
		for (String x : targets) {
			if (hammingDist(s, x) > max_d) {
				max_s = x;
				max_d = hammingDist(s, x);
			}
		}
		return max_s;
	}
	
	public static ArrayList<String> findNearestK(String s, String[] targets, int k){
		ArrayList<String> res = new ArrayList<>();
		for (String x : targets) {
			if (hammingDist(s, x) <= k) {
				res.add(x);
			}
		}
		return res;
	}
	
	public static int stringDist(String left, String right) {
		int diff = 0;
		if (left.length() > right.length()) {
			diff = left.length() - right.length();
			left = left.substring(0, right.length());
		}
		else if (left.length() < right.length()) {
			diff = right.length() - left.length();
			right = right.substring(0, left.length());
		}
		return diff + hammingDist(left, right);
	}
}
