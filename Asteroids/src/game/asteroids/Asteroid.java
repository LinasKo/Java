package game.asteroids;

import java.util.Random;

public class Asteroid {
	private double x, y, speedX, speedY;
	private double SPEED_MAX = 8;
	private boolean gone = false;

	public Asteroid() {
		Random rnd = new Random();
		int random_result = rnd.nextInt(4);
		switch (random_result) {
		case 0: // left
			this.x = 0;
			this.y = rnd.nextInt(Primary.HEIGHT);
			this.speedX = rnd.nextDouble() * SPEED_MAX;
			this.speedY = (rnd.nextDouble() - 0.5) * SPEED_MAX * 2;
			break;

		case 1: // right
			this.x = Primary.WIDTH;
			this.y = rnd.nextInt(Primary.HEIGHT);
			this.speedX = -rnd.nextDouble() * SPEED_MAX;
			this.speedY = (rnd.nextDouble() - 0.5) * SPEED_MAX * 2;
			break;

		case 2: // top
			this.x = rnd.nextInt(Primary.WIDTH);
			this.y = 0;
			this.speedX = (rnd.nextDouble() - 0.5) * SPEED_MAX * 2;
			this.speedY = rnd.nextDouble() * SPEED_MAX;
			break;

		case 3: // bottom
			this.x = rnd.nextInt(Primary.WIDTH);
			this.y = Primary.HEIGHT;
			this.speedX = (rnd.nextDouble() - 0.5) * SPEED_MAX * 2;
			this.speedY = -rnd.nextDouble() * SPEED_MAX;
			break;
		}
	}

	public void update() {
		x += speedX;
		y += speedY;

		if (x < 0)
			gone = true;
		else if (x > Primary.WIDTH)
			gone = true;
		else if (y < 0)
			gone = true;
		else if (y > Primary.HEIGHT)
			gone = true;
	}

	public boolean isGone() {
		return gone;
	}

	public void setGone(boolean gone) {
		this.gone = gone;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}
}
