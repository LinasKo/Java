package game.asteroids;

import java.util.ArrayList;

public class Ship {

	private double x, y, speedX, speedY;
	private double orientation;

	private double FLIGHT_SPEED = 1;
	private double TURNING_RATE = 0.1;
	private double SPEED_DECAY = 0.9;
	private double DEACCELERATION_CONSTANT = 0.5;
	private double PROJECTILE_SPEED = 10;

	private boolean accelerating = false;
	private boolean deacellerating = false;
	private boolean turning_left = false;
	private boolean turning_right = false;

	private static ArrayList<Projectile> projectiles = new ArrayList<Projectile>();

	public Ship(int x, int y) {
		this.x = x;
		this.y = y;
		speedX = 0;
		speedY = 0;
		orientation = Math.PI / 2; // Facing up
	}

	public void update() {
		// Turn
		if (turning_left) {
			if (!turning_right) {
				orientation += TURNING_RATE;
			}
		} else if (turning_right) {
			if (!turning_left) {
				orientation -= TURNING_RATE;
			}
		}

		// Determine sin/cos coefficients for speed application
		double coeffX = Math.cos(orientation);
		double coeffY = Math.sin(orientation);

		// Base speed decay.
		speedX *= SPEED_DECAY;
		speedY *= SPEED_DECAY;

		// Acceleration and deacelleration;
		if (accelerating) {
			speedX += FLIGHT_SPEED * coeffX;
			speedY += FLIGHT_SPEED * coeffY;
		}
		if (deacellerating) { // deaccelerating is slower than accelerating D_C
								// times.
			speedX -= FLIGHT_SPEED * DEACCELERATION_CONSTANT * coeffX;
			speedY -= FLIGHT_SPEED * DEACCELERATION_CONSTANT * coeffY;
		}

		x += speedX;
		if (x > Primary.WIDTH) {
			x = Primary.WIDTH;
			speedX = 0;
		}
		if (x < 0) {
			x = 0;
			speedX = 0;
		}

		y -= speedY; // Down is up
		if (y > Primary.HEIGHT) {
			y = Primary.HEIGHT;
			speedY = 0;
		}
		if (y < 0) {
			y = 0;
			speedY = 0;
		}
	}

	public void shoot() {
		if (Primary.can_shoot) {
			Projectile p = new Projectile(x + (Primary.SHIP_WIDTH / 2), y
					+ (Primary.SHIP_HEIGHT / 2), PROJECTILE_SPEED, orientation);
			projectiles.add(p);
		}
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getSpeedX() {
		return speedX;
	}

	public double getSpeedY() {
		return speedY;
	}

	public double getOrientation() {
		return orientation;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setSpeedX(double speedX) {
		this.speedX = speedX;
	}

	public void setSpeedY(double speedY) {
		this.speedY = speedY;
	}

	public void setOrientation(double orientation) {
		this.orientation = orientation;
	}

	public void setAccelerating(boolean accelerating) {
		this.accelerating = accelerating;
	}

	public void setDecellerating(boolean decellerating) {
		this.deacellerating = decellerating;
	}

	public void setTurning_left(boolean turning_left) {
		this.turning_left = turning_left;
	}

	public void setTurning_right(boolean turning_right) {
		this.turning_right = turning_right;
	}

	public static ArrayList<Projectile> getProjectiles() {
		return projectiles;
	}

	public static void setProjectiles(ArrayList<Projectile> projectiles) {
		Ship.projectiles = projectiles;
	}
}
