package game.asteroids;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class Primary extends Applet implements Runnable, KeyListener {

	public static boolean sandbox = false; // God mode

	public static int WIDTH = 1200;
	public static int HEIGHT = 600;
	public static int SHIP_HEIGHT = 12;
	public static int SHIP_WIDTH = 8;
	public static int PROJECTILE_SIZE = 5;
	public static int ASTEROID_RADIUS = 8;
	public static int STARTING_DIVISOR = 25; // The lower the number, the faster
	// asteroids spawn
	public static int RETICULE_DISTANCE = 90;
	public static int RETICULE_RADIUS = 6;
	public static int DIFFICULTY_COEFFICIENT = 150; // The lower this is, the
													// faster difficulty
													// increases
	private boolean not_dead = true;
	public static boolean can_shoot = true;

	private int counter = 0;
	private int divisor = STARTING_DIVISOR;

	private static Ship ship;
	private ArrayList<Asteroid> asteroids;

	// Double buffering
	private Image image;
	private Graphics second;
	private static Graphics2D g2;

	@Override
	public void init() {
		// Create basic window
		setSize(WIDTH, HEIGHT);
		setBackground(Color.BLACK);
		setFocusable(true);
		addKeyListener(this);

		// Set title
		Frame frame = (Frame) this.getParent().getParent();
		frame.setTitle("Asteroids - LKM7");

		// Create ship
		ship = new Ship(WIDTH / 2, HEIGHT / 2);

		// Create asteroid array;
		asteroids = new ArrayList<Asteroid>();
	}

	@Override
	public void start() {
		Thread thread = new Thread(this);
		thread.start();
	}

	@Override
	public void stop() {
	}

	@Override
	public void destroy() {
	}

	@Override
	public void run() {
		while (not_dead) {
			// fancy mancy difficulty timer
			counter = (counter + 1) % DIFFICULTY_COEFFICIENT;
			if (counter == 0 && divisor != 1)
				divisor--;

			ship.update();
			repaint();

			// Implement ticks
			try {
				Thread.sleep(17);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			ArrayList<Projectile> projectiles = ship.getProjectiles();
			for (int i = 0; i < projectiles.size(); i++) {
				Projectile p = projectiles.get(i);
				p.update();
				if (p.isGone())
					projectiles.remove(i);
			}

			// Spawn asteroids
			if (counter % divisor == 0) {
				Asteroid ast = new Asteroid();
				asteroids.add(ast);
			}
			// Update asteroids
			for (int i = 0; i < asteroids.size(); i++) {
				Asteroid a = asteroids.get(i);
				a.update();
				int action = checkCollision(a);
				if (a.isGone() || action == 1 || action == 2)
					asteroids.remove(i);
				if (action == 1)
					if (!sandbox)
						not_dead = false;
			}
		}
	}

	private int checkCollision(Asteroid a) {
		// Deal with ship - asteroid collision
		double distance_a_ship = Math.sqrt(Math.pow(ship.getX() + SHIP_WIDTH
				/ 2 - a.getX() - ASTEROID_RADIUS, 2)
				+ Math.pow(ship.getY() + SHIP_HEIGHT / 2 - a.getY()
						- ASTEROID_RADIUS, 2));
		if (distance_a_ship <= ASTEROID_RADIUS + SHIP_WIDTH / 2) {
			return 1;
		}
		// Deal with projectiles
		ArrayList<Projectile> projectiles = ship.getProjectiles();
		for (int i = 0; i < projectiles.size(); i++) {
			Projectile p = projectiles.get(i);
			double distance_a_proj = Math.sqrt(Math.pow(a.getX()
					+ ASTEROID_RADIUS - p.getX(), 2)
					+ Math.pow(a.getY() + ASTEROID_RADIUS - p.getY(), 2));
			if (distance_a_proj <= ASTEROID_RADIUS) {
				projectiles.remove(i);
				return 2;
			}
		}
		return 0;
	}

	@Override
	public void update(Graphics g) {
		// Double buffering
		if (image == null) {
			image = createImage(this.getWidth(), this.getHeight());
			second = image.getGraphics();
		}
		second.setColor(getBackground());
		second.fillRect(0, 0, getWidth(), getHeight());
		second.setColor(getForeground());
		paint(second);

		g.drawImage(image, 0, 0, this);
	}

	@Override
	public void paint(Graphics g) {
		g2 = (Graphics2D) g;
		g2.setColor(Color.WHITE);
		Rectangle2D ship_box = new Rectangle2D.Double(ship.getX(), ship.getY(),
				SHIP_WIDTH, SHIP_HEIGHT);
		g2.draw(ship_box);

		g.setColor(Color.RED);
		int retX = (int) (ship.getX() + SHIP_WIDTH / 2 + RETICULE_DISTANCE
				* Math.cos(ship.getOrientation()));
		int retY = (int) (ship.getY() + SHIP_HEIGHT / 2 - RETICULE_DISTANCE
				* Math.sin(ship.getOrientation()));
		g.fillOval(retX, retY, RETICULE_RADIUS, RETICULE_RADIUS);

		g.setColor(Color.YELLOW);
		for (Projectile p : ship.getProjectiles()) {
			g.fillRect((int) p.getX(), (int) p.getY(), PROJECTILE_SIZE,
					PROJECTILE_SIZE);
		}

		g.setColor(Color.WHITE);
		for (Asteroid a : asteroids) {
			g.fillOval((int) a.getX(), (int) a.getY(), ASTEROID_RADIUS * 2,
					ASTEROID_RADIUS * 2);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			ship.setAccelerating(true);
			break;
		case KeyEvent.VK_DOWN:
			ship.setDecellerating(true);
			break;
		case KeyEvent.VK_LEFT:
			ship.setTurning_left(true);
			break;
		case KeyEvent.VK_RIGHT:
			ship.setTurning_right(true);
			break;
		case KeyEvent.VK_SPACE:
			ship.shoot();
			can_shoot = false;
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			ship.setAccelerating(false);
			break;
		case KeyEvent.VK_DOWN:
			ship.setDecellerating(false);
			break;
		case KeyEvent.VK_LEFT:
			ship.setTurning_left(false);
			break;
		case KeyEvent.VK_RIGHT:
			ship.setTurning_right(false);
			break;
		case KeyEvent.VK_SPACE:
			can_shoot = true;
			break;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	public Graphics2D getG2() {
		return g2;
	}
}
