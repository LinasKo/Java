package game.asteroids;

public class Projectile {
	private boolean gone = false;

	private double x;
	private double y;
	private double speed;
	private double orientation;

	public Projectile(double x, double y, double speed, double orientation) {
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.orientation = orientation;
	}

	public void update() {
		double coeffX = Math.cos(orientation);
		double coeffY = Math.sin(orientation);

		x += speed * coeffX;
		y -= speed * coeffY;

		if (x < 0)
			gone = true;
		else if (x > Primary.WIDTH)
			gone = true;
		else if (y < 0)
			gone = true;
		else if (y > Primary.HEIGHT)
			gone = true;
	}

	public boolean isGone() {
		return gone;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
}
