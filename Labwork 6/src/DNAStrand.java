
public class DNAStrand {

	private String dna;
	
	public DNAStrand(String dna) {
		this.dna = dna;
	}

	public boolean isValidDNA() {
		if (this.dna == "") return false;
		for (int i = 0; i < this.dna.length(); i++) {
			char c = dna.charAt(i);
			if (c != 'A' && c != 'T' && c != 'C' && c != 'G') {
				return false;
			}
		}
		return true;
	}
	
	public String complementWC() {
		String res = "";
		for (int i = 0; i < this.dna.length(); i++) {
			char c = this.dna.charAt(i);
			     if (c == 'A') res += 'T';
			else if (c == 'T') res += 'A';
			else if (c == 'C') res += 'G';
			else if (c == 'G') res += 'C';
		}
		return res;
	}
	
	public String palindromeWC() {
		String res = "";
		for (int i = this.dna.length()-1; i >= 0 ; i--) {
			res += this.complementWC().charAt(i);
		}
		return res;
	}
	
	public boolean containsSequence(String seq) {
		int len = seq.length();
		for (int i = 0; i <= (this.dna.length() - len); i++) {
			int aux = 0;
			for (int j = 0; j < len; j++) {
				if (this.dna.charAt(i+j) == seq.charAt(j)) {
					aux += 1;
					if (aux == len) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public String toString() {
		return this.dna;
	}
	
	public static void main(String[] args) {
		;
	}
}
