
public class Favourites {

	private MusicTrack[] favourites = new MusicTrack[5];
	
	//this statement
	public void addTrack(String artist, String title) {
		for (int i = 0; i < this.favourites.length; i++) {
			MusicTrack mt = new MusicTrack(artist, title);
			if (this.favourites[i] == null) {
				this.favourites[i] = mt;
				break;
			}
			else if (i == this.favourites.length-1) {
				System.out.print("Sorry, can't add: " + mt.toString() + "\n\n");
			}
		}
	}
	
	public void showFavourites() {
		for (int i = 0; i < this.favourites.length; i++) {
			if (this.favourites[i] != null) {
				System.out.print(this.favourites[i].toString() + "\n");
			}
		}
	}
	
	public static void main(String[] args) {
		Favourites favourites = new Favourites();
		favourites.addTrack("Fun", "Some Nights");
		favourites.addTrack("Oliver Tank", "Help You Breathe");
		favourites.addTrack("Horse Feathers", "Fit Against the Country");
		favourites.addTrack("Emile Sande", "Country House");
		favourites.addTrack("Fun", "Walking the Dog");
		favourites.addTrack("Porcelain Raft", "Put Me To Sleep");
		favourites.showFavourites();
	}
}
