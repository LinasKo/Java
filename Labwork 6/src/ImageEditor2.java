import java.awt.Color;


public class ImageEditor2 {

	public static Picture threshold(Picture p, int thresh) {
	Picture poc = new Picture(p.width(), p.height());
		for (int i = 0; i < p.width(); i++) {
			for (int j = 0; j < p.height(); j++) {
				if (ImageEditor1.luminance(p.get(i, j)) < thresh)
					poc.set(i, j, Color.black);
				else {
					poc.set(i, j, ImageEditor1.toGrey(p.get(i, j)));
				}
			}
		}
		return poc;
	}
	
	public static void main(String[] args) {
		//Picture t = threshold(p, 120);
		//t.show();
	}
}
