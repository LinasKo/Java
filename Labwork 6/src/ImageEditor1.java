import java.awt.Color;
//import Picture;

public class ImageEditor1 {

	public static double luminance(Color color) {
		return 0.299*color.getRed() + 0.587*color.getGreen() + 0.114*color.getBlue();
	}
	
	public static Color toGrey(Color color) {
		return new Color((int) (luminance(color)+0.5), (int) (luminance(color)+0.5), (int) (luminance(color)+0.5));
	}
	
	public static Picture makeGreyscale(Picture pic) {
		Picture poc = new Picture(pic.width(), pic.height());
		for (int i = 0; i < pic.width(); i++) {
			for (int j = 0; j < pic.height(); j++) {
				poc.set(i, j, toGrey(pic.get(i, j)));
			}
		}
		return poc;
	}
	
	public static void main(String[] args) {
	}

}
