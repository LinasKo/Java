import java.util.Calendar;

public class CreditCard {
	
	Calendar now = Calendar.getInstance();

	private int expiryMonth;
	private int expiryYear;
	private String firstName, lastName, ccNumber;
	
	public CreditCard(int expiryMonth, int expiryYear, String firstName, String lastName, String ccNumber) {
		this.expiryMonth = expiryMonth;
		this.expiryYear = expiryYear;
		this.firstName = firstName;
		this.lastName = lastName;
		this.ccNumber = ccNumber;
	}
	
	public String formatExpiryDate() {
		return "" + this.expiryMonth + "/" + this.expiryYear % 100;
	}
	
	public String formatFullName() {
		return this.firstName + " " + this.lastName;
	}
	
	public String formatCCNumber() {
		String res = "";
		for (int i = 0; i < this.ccNumber.length(); i++) {
			res += this.ccNumber.charAt(i);
			if ((i+1) % 4 == 0 && i != this.ccNumber.length()-1) res += " ";
		}
		return res;
	}
	
	public boolean isValid() {
		int ey = this.expiryYear;
		int em = this.expiryMonth;
		int cy = now.get(Calendar.YEAR);
		int cm = now.get(Calendar.MONTH);
		if (cy < ey) {
			return true;
		}
		else if (cy == ey && (cm+1) < em) {
			return true;
		}
		return false;
	}
	
	public String toString() {
		String res;
		res = "Number: " + formatCCNumber() + "\n" + "Expiration date: " + formatExpiryDate() + "\n";
		res += "Account holder: " + formatFullName() + "\n" + "Is valid: " + isValid();
		return res;
	}
	
	public static void main(String[] args) {
		CreditCard cc1 = new CreditCard(10, 2014, "Bob", "Jones", "1234567890123456");
		cc1.isValid();
		
	}
}
