import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

public class TrianglePath {

	public static void main(String[] args) {

		// Input
		String fileName = args[0];
		int[][] tri = null;
		String line;
		int lines; // Will use this for triangle size throughout the program
		boolean noErrorFlag = true;
		boolean testFlag = false; // Change this to true if you want to
									// test for speed. This will generate a
									// random non-negative 500x500 array and
									// print the output.

		// Input
		if (!testFlag) {
			// Count the lines
			lines = 0;
			try {
				BufferedReader ln = new BufferedReader(new FileReader(fileName));
				while ((line = ln.readLine()) != null) {
					lines++;
				}
				ln.close();
			} catch (IOException e) {
				System.out.println("File not found. Please put it next to the java file.");
				noErrorFlag = false;
			}

			// Create the main triangle array
			try {
				BufferedReader in = new BufferedReader(new FileReader(fileName));
				tri = new int[lines][lines];
				int row = 0;
				String[] pieces;
				while ((line = in.readLine()) != null) {
					pieces = line.split("\\s+");
					if (pieces.length <= row) {
						System.out.println("Line " + (row+1) + " has too few values");
						noErrorFlag = false;
					}
					else if (pieces.length > row+1) {
						System.out.println("Line " + (row+1) + " has too many values");
						noErrorFlag = false;
					}
					int col = 0;
					while (col < lines) {
						if (col < pieces.length)
							try{
								tri[row][col] = Integer.parseInt(pieces[col]);
								if (tri[row][col] < 0) {
									System.out.println("The value The value at (row,column):(" + row + "," + col + ") is negative.");
									noErrorFlag = false;
								}
							}
						catch(NumberFormatException e) {
							System.out.println("The value at (row,column):(" + row + "," + col + ") is not an integer.");
							noErrorFlag = false;
						}
						else
							tri[row][col] = -1; // Will help find correct paths
												// where the values can be 0.
						// TODO check if target is integer
						col++;
					}

					row++;
				}
				in.close();
			} catch (IOException e) {
				noErrorFlag = false; // Just to be sure
			}
		}

		// Speed testing. The program either reads the .txt file or uses this bit.
		// The test give only the general idea about the speed.
		// It does not calculate actual running time.
		else {
			lines = 500;
			Random r = new Random();
			tri = new int[lines][lines];
			for (int i = 0; i < 500; i++) {
				for (int j = 0; j < 500; j++) {
					tri[i][j] = Math.abs(r.nextInt());
				}
			}
		}

		// Create helper arrays
		if (noErrorFlag) {
			// Will be useful for outputting results
			int[][] old_triangle = new int[lines][lines];
			// Will show the path trace
			int[][] directions = new int[lines][lines];

			for (int i = 0; i < lines; i++) {
				for (int j = 0; j < lines; j++) {
					directions[i][j] = 2;
					old_triangle[i][j] = tri[i][j];
				}
			}

			// Calculations
			for (int row = lines - 2; row >= 0; row--) {
				for (int col = 0; col <= row; col++) {
					int least = Math.min(tri[row + 1][col],
							tri[row + 1][col + 1]);
					tri[row][col] += least;
					if (least == tri[row + 1][col]) {
						directions[row][col] = 0;
					} else
						directions[row][col] = 1;
				}
			}

			// Output
			int x = 0;
			int y = 0;
			System.out.print("Minimal path is: ");
			while (x < lines - 1) {
				System.out.print(old_triangle[x][y] + " + ");
				y += directions[x][y];
				x++;
			}
			System.out.print(old_triangle[x][y] + " = " + tri[0][0]);
		}
	}
}
