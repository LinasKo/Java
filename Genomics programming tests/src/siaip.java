
public class siaip {

	public static void main(String[] args) {
		double a = 2.134345343234;
		System.out.println(a);
		System.out.printf("%.2f", a);
		System.out.println();
		System.out.printf("%.7f", a);
		System.out.println();
		System.out.printf("%.15f", a);
		System.out.println();
		
		System.out.format("gibberish, %s and some other %s.", "trash", "stuff");
		System.out.println();
		String b = String.format("some text %s %s %s %S", "greed", a, "ghosts", a*2);
		System.out.println(b);
	
		
	}
	
	public static double arrayMax(double[] data) {
		
		if(data.length == 0) {
			return -1;
		}
		double max = data[0];
		for(int i = 0; i < data.length; i++) {
			if (data[i] > max) {
				max = data[i];
			}
		}
		return max;
	}
	
	public static double[] normalize(double[] data) {
		double sum = 0;
		for(double d : data) {
			sum += d;
		}
		
		for (int i = 0; i < data.length; i++) {
			data[i] /= sum;
		}
		return data;
	}
	
	public static double[] reverse(double[] data) {
		double[] tmp = new double[data.length];
		for (int i = 0; i < data.length; i++) {
			tmp[i] = data[data.length - i - 1];
		}
		return tmp;
	}
	
	public static void reverseInPlace(double[] data) {
		double[] tmp = reverse(data);
		
		for (int i = 0; i < data.length; i++) {
			data[i] = tmp[i];
		}
	
	
	double[] cd = new double[]{1.54, 8.54};
	double[] de = reverse(cd);
	reverseInPlace(cd);
	}
}
