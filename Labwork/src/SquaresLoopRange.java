
public class SquaresLoopRange {

	public static void main(String[] args) {
		int start_number = Integer.parseInt(args[0]);
		int stop_number = Integer.parseInt(args[1]);
		if (start_number > stop_number) {
			System.out.println("Start-limit greater than stop-limit!");
		}
		else {
			for (int i = start_number; i <= stop_number; i++){
				System.out.print(i*i + " ");
			}
		}
	}
}
