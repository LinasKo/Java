
public class MultiplesLoopRange {

	public static void main(String[] args) {
		int start_number = Integer.parseInt(args[0]);
		int stop_number = Integer.parseInt(args[1]);
		int multiples = Integer.parseInt(args[2]);
		if (start_number > stop_number) {
			for (int i = start_number; i >= stop_number; i--){
				if (i % multiples == 0) System.out.print(i + " ");
			}
		}
		else {
			for (int i = start_number; i <= stop_number; i++){
				if (i % multiples == 0) System.out.print(i + " ");
			}
		}
	}
}
