
public class ImQuadraticSolver {

	public static void main(String[] args) {
		double a = Double.parseDouble(args[0]);
		double b = Double.parseDouble(args[1]);
		double c = Double.parseDouble(args[2]);
		double d = b*b - 4*a*c;
		if (a == 0) {
			System.out.println(-c / b);
		}
		else {
			if (d < 0) {
				System.out.println(-b/2/a + " + " + Math.sqrt(Math.abs(d))/2/a + "i");
				System.out.println(-b/2/a + " - " + Math.sqrt(Math.abs(d))/2/a + "i");
			}
			else {
				System.out.println((-b + Math.sqrt(d))/2/a);
				System.out.println((-b - Math.sqrt(d))/2/a);
			}
		}
	}
}
