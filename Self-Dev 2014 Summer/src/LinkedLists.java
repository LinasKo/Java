import java.util.ArrayList;


public class LinkedLists {
	
	public static void main(String[] args) {
		NodeL chain = NodeL.MakeList(new int[] {1,2,3,4,5,6,1,2,8,9});
		System.out.println(NodeL.PrintOut(chain));
		NodeL.DeleteDuplicates(chain);
		System.out.println(NodeL.PrintOut(chain));
		System.out.println();
		System.out.println(NodeL.FindKth(chain, 3).data);
		NodeL.DelMidNode(NodeL.GetNode(chain, 2));
		System.out.println(NodeL.PrintOut(chain));
		System.out.println("Deletions:");
		NodeL shortList = NodeL.MakeList(new int[] {1,2,3});
		NodeL.DelMidNode(NodeL.GetNode(shortList, 2));
		System.out.println(NodeL.PrintOut(shortList));
		System.out.println("OK, it ain't possible to delete the laste element by setting it to null.");
		System.out.println();
		
		NodeL mixup = NodeL.MakeList(new int[] {8,6,7,1,5,3,2,4,9,6,5,1,2,8});
		System.out.println(NodeL.PrintOut(mixup));
		mixup = NodeL.PartitionList(mixup, 3);
		System.out.println(NodeL.PrintOut(mixup));
		System.out.println();
		
		NodeL n1 = NodeL.MakeList(new int[] {5,7,8,9,6,4,5,2});
		NodeL n2 = NodeL.MakeList(new int[] {1,9,8,6,5,7,0});
		System.out.println(NodeL.AddLists(n1, n2));
		System.out.println(25469875 + 756891);
		System.out.println();
		
		NodeL.AppendToBack(n1, n1.next.next.next);
		System.out.print(n1.next.next.next.data);
		System.out.println(" " + NodeL.Decircle(n1).data);
		System.out.println();
		
		NodeL k1 = NodeL.MakeList(new int[] {1,2,4,5,6,4,2,1});
		NodeL k2 = NodeL.MakeList(new int[] {1,2,2,2,1});
		System.out.println(NodeL.Palindrome(k1) + " " + NodeL.Palindrome(k2));
	}

}
class NodeL {
	int data;
	NodeL next = null;
	
	public NodeL(int data) {
		this.data = data;
	}
	
	public static NodeL AppendToBack(NodeL chain, NodeL x) {
		NodeL head = chain;
		while (chain.next != null) {
			chain = chain.next;
		}
		chain.next = x;
		return head;
	}
	
	public static NodeL MakeList(int[] content) {
		NodeL n = new NodeL(content[0]);
		for (int i = 1; i < content.length; i++) {
			AppendToBack(n, new NodeL(content[i]));
		}
		return n;
	}
	
	public static String PrintOut(NodeL n) {
		StringBuffer sb = new StringBuffer();
		while (n.next != null) {
			sb.append(n.data + " ");
			n = n.next;
		}
		sb.append(n.data + " ");
		return sb.toString();
	}
	
	public static NodeL GetNode(NodeL n, int index) {
		for (int i = 0; i < index; i++) {
			n = n.next;
		}
		return n;
	}
	
	public static NodeL DeleteDuplicates(NodeL head) {
		NodeL n = head;
		while (n.next != null) {
			int delete_value = n.data;
			NodeL curr = n;
			NodeL next = curr.next;
			while (true) {
				if (next.data == delete_value) {
					if (next.next == null) {
						curr.next = null;
						break;
					}
					else {
						curr.next = next.next;
					}
				}
				else if (next.next == null) {
					break;
				}
				curr = curr.next;
				next = next.next;
			}
			n = n.next;
		}
		return head;
	}
	
	public static NodeL FindKth(NodeL n, int k) {
		NodeL head = n;
		int len = 1;
		while (n.next != null) {
			n = n.next;
			len++;		
		}
		n = head;
		int current = 0;
		while (current < len - k) {
			n = n.next;
			current++;
		}
		return n;
	}
	
	public static void DelMidNode(NodeL n) {
		if (n.next == null) {
			n = null;
		}
		else {
			n.data = n.next.data;
			n.next = n.next.next;
		}
	}
	
	public static NodeL PartitionList(NodeL head, int x) {
		NodeL n = head;
		ArrayList<NodeL> lessers = new ArrayList<NodeL>();
		ArrayList<NodeL> greatersOrX = new ArrayList<NodeL>();
		while (n != null) {
			if (n.data < x) {
				lessers.add(n);
			}
			else {
				greatersOrX.add(n);
			}
			n = n.next;
		}
		lessers.addAll(greatersOrX);
		for (int i = 0; i < lessers.size()-1; i++) {
			lessers.get(i).next = lessers.get(i+1);
		}
		lessers.get(lessers.size()-1).next = null;
		return lessers.get(0);
	}
	
	public static int AddLists(NodeL n1, NodeL n2) {
		if (n1 == null && n2 == null) {
			return 0;
		}
		else if (n1 == null) {
			return n2.data + 10 * AddLists(null, n2.next);
		}
		else if (n2 == null) {
			return n1.data + 10 * AddLists(n1.next, null);
		}
		else {
			return n1.data + n2.data + 10 * AddLists(n1.next, n2.next);
		}
	}
	
	public static NodeL Decircle(NodeL head) {
		NodeL n = head;
		ArrayList<NodeL> nodes = new ArrayList<NodeL>();
		while(n != null) {
			if (nodes.contains(n)) {
				return n;
			}
			else {
				nodes.add(n);
			}
			n = n.next;
		}
		return n;
	}
	
	public static boolean Palindrome(NodeL head) {
		NodeL n = head;
		ArrayList<Integer> values = new ArrayList<Integer>();
		while(n != null) {
			values.add(n.data);
			n = n.next;
		}
		for (int i = 0; i <= values.size()/2; i++) {
			if (values.get(i) != values.get(values.size() - i - 1)) {
				return false;
			}
		}
		return true;
	}
}
