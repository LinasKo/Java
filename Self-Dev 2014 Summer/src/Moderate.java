public class Moderate {

	public static void main(String[] args) {

		TicTacToe table1 = new TicTacToe(new int[][] { { 1, 2, 1 }, { 2, 1, 2 }, { 2, 2, 1 } });
		TicTacToe table2 = new TicTacToe(new int[][] { { 1, 2, 1 }, { 2, 1, 2 }, { 2, 1, 2 } });
		System.out.println(table1.checkWin());
		System.out.println(table2.checkWin());
		System.out.println();

		System.out.println(trailingZeroesInFactorial(4));
		System.out.println(trailingZeroesInFactorial(101));
		System.out.println();

		System.out.println(hits("GREGK", "GREED")[0]);
		System.out.println(hits("GREGK", "GREED")[1]);
		System.out.println();

		System.out.println(sortingBounds(new int[] { 1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 15, 16, 18, 19 })[0]);
		System.out.println(sortingBounds(new int[] { 1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 15, 16, 18, 19 })[1]);
		System.out.println();
		
		System.out.println(printArr(largestSequence(new int[]{2,-8,3,-2,4,-10})));
	}

	public static int[] largestSequence(int[] seq) {
		int[] res_seq = {seq[0]};
		int max = seq[0];
		for (int start = 0; start < seq.length; start++) {
			for (int more = 0; more < seq.length-start; more++) {
				
				int sum = seq[start];
				int[] tmp_seq = new int[seq.length];
				tmp_seq[0] = sum;
				for (int i = 1; i <= more; i++) {
					sum += seq[start+i];
					tmp_seq[i] = seq[start+i];
				}
				if (sum > max) {
					max = sum;
					res_seq = tmp_seq;
				}
				
			}
		}
		return res_seq;
	}
	
	public static String printArr(int[] arr) {
		StringBuffer sb = new StringBuffer();
		for (int i : arr) {
			sb.append(i + " ");
		}
		return sb.toString();
	}
	
	public static int[] sortingBounds(int[] array) {
		int[] res = { -1, -1 };
		for (int i = 0; i < array.length - 1; i++) {
			int left_val = array[i];
			if (res[0] == -1) {
				for (int j = i + 1; j < array.length; j++) {
					if (array[j] < left_val || (array[j] == left_val && j != i + 1)) {
						res[0] = i;
						break;
					}
				}
			}
			int right_val = array[array.length - 1 - i];
			if (res[1] == -1) {
				for (int j = array.length - 2 - i; j >= 0; j--) {
					if (array[j] > right_val || (array[j] == right_val && j != i - 1)) {
						res[1] = array.length - 1 - i;
						break;
					}
				}
			}
			if (res[0] != -1 && res[1] != -1) {
				break;
			}
		}
		return res;
	}

	public static int[] hits(String guess, String answer) {
		int[] res = new int[2];
		for (int i = 0; i < answer.length(); i++) {
			char c = guess.charAt(i);
			if (answer.charAt(i) == c) {
				res[0]++;
			} else if (answer.contains("" + c)) {
				res[1]++;
			}
		}
		return res;
	}

	public static int trailingZeroesInFactorial(int n) {
		int res = 0;
		int x = 1;
		int mult = 1;
		while (x > 0) {
			res += x = (int) (n / Math.pow(5, mult));
			mult++;
		}
		return res;
	}

}

class TicTacToe {
	int[][] table = new int[3][3];

	public TicTacToe(int[][] table) {
		this.table = table;
	}

	public boolean checkWin() {
		// Check rows and cols
		int first;
		for (int i = 0; i < 3; i++) {
			first = table[i][0];
			if (first != 0) {
				if (table[i][1] == first && first == table[i][2]) {
					return true;
				}
			}
			first = table[0][i];
			if (first != 0) {
				if (table[1][i] == first && first == table[2][i]) {
					return true;
				}
			}
		}
		first = table[0][0];
		if (first == table[1][1] && first == table[2][2] && first != 0) {
			return true;
		}
		first = table[2][0];
		if (first == table[1][1] && first == table[0][2] && first != 0) {
			return true;
		}
		return false;
	}
}