import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.HashMap;


class NodeD {
	int data;
	ArrayList<NodeD> children;
	
	public NodeD(int data) {
		this.data = data;
		children = new ArrayList<NodeD>();
	}
	
	public String toString() {
		if (children.size() == 0) {
			return "" + data;
		}
		else {
			String s = data + ":(";
			
			for (NodeD n : children) {
				s += n.toString() + ", ";
			}
			return s.subSequence(0, s.length()-2) + ")";
		}
	}
}

public class TreesAndGraphs {
	
	static int max_depth = -1;
	static int min_depth = -1;
	
	public static boolean searchWithReset(Tree t) {
		boolean b = isBalanced(t.root, 0);
		max_depth = -1;
		min_depth = -1;
		return b;
	}
	
	public static Tree addToTree(int data, Tree t, int[] directions){
		NodeD n = t.root;
		for (int d : directions) {
			if (d >= n.children.size()) {
				return null;
			}
			else {
				n = n.children.get(d);
			}
		}
		n.children.add(new NodeD(data));
		return t;
	}
	
	public static boolean isBalanced(NodeD n, int depth) {
		if (n.children.size() == 0) {
			if (depth < min_depth || min_depth == -1) {
				min_depth = depth;
			}
			if (depth > max_depth || max_depth == -1) {
				max_depth = depth;
			}
			if (max_depth - min_depth >= 2) {
				return false;
			}
			else {
				return true;
			}
		}
		else {
			boolean tmp = true;
			for (NodeD child : n.children) {
				tmp &= isBalanced(child, depth+1);
			}
			return tmp;
		}
	}
	
	private static NodeD firstNodeFromHashMap(HashMap<NodeD, Integer> hm) {
		return (NodeD) hm.keySet().toArray()[0];
	}
	
	public static ArrayList<Node> linkedLists(Tree t) {
		ArrayList<Node> res = new ArrayList<Node>();
		HashMap<NodeD, Integer> toVisit = new HashMap<NodeD, Integer>();
		NodeD n = t.root;
		int depth;
		toVisit.put(n, 0);
		while (!toVisit.isEmpty()) {
			n = firstNodeFromHashMap(toVisit);
			depth = toVisit.get(n);
			if (res.size() <= toVisit.get(n)) {
				res.add(new Node(n.data));
			}
			else {
				Node.AppendToBack(res.get(toVisit.get(n)), new Node(n.data));
			}
			for (NodeD child : n.children) {
				toVisit.put(child, depth+1);
			}
			toVisit.remove(n);
		}
		return res;
	}
	
	public static boolean isBinarySearchTree(NodeD n) {
		if (n.children.size() >= 3) {
			return false;
		} 
		else if (n.children.size() == 1) {
			return true & isBinarySearchTree(n.children.get(0));
		}
		else if (n.children.size() == 0) {
			return true;
		}
		else if (n.children.get(0).data < n.data && n.children.get(1).data >= n.data) {
			boolean b1 = isBinarySearchTree(n.children.get(0));
			boolean b2 = isBinarySearchTree(n.children.get(1));
			return true & b1 & b2;
		}
		else {
			return false;
		}
	}
	
	public static void main(String[] args) {
		
		Tree t = new Tree(0);
		TreesAndGraphs.addToTree(1, t, new int[] {});
		TreesAndGraphs.addToTree(2, t, new int[] {});
		TreesAndGraphs.addToTree(11, t, new int[] {0});
		TreesAndGraphs.addToTree(7, t, new int[] {0, 0});
		TreesAndGraphs.addToTree(11, t, new int[] {0});
		TreesAndGraphs.addToTree(12, t, new int[] {1});
		TreesAndGraphs.addToTree(22, t, new int[] {1});
		System.out.println(t.root.toString());
		System.out.println(TreesAndGraphs.searchWithReset(t));
		TreesAndGraphs.addToTree(743, t, new int[] {0, 0, 0});
		System.out.println(t.root.toString());
		System.out.println(TreesAndGraphs.searchWithReset(t));
		System.out.println();
		
		System.out.println("nodes:");
		ArrayList<Node> nodeified = TreesAndGraphs.linkedLists(t);
		for (Node n : nodeified) {
			System.out.println(Node.PrintOut(n));
		}
		System.out.println();
		System.out.println(TreesAndGraphs.isBinarySearchTree(t.root));
	}
}
