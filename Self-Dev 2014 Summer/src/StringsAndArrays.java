import java.util.Arrays;
import java.util.HashMap;


public class StringsAndArrays {
	
	private static String PrintMatrix(int[][] matrix) {
		String s = "";
		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[0].length; col++) {
				s += matrix[row][col] + " ";
			}
			s += "\n";
		}
		return s;
	}

	public static boolean UniqueCharWithDataStructures(String sentence) {
		HashMap<Character, Integer> charMap = new HashMap<Character, Integer>();
		for (Character c : sentence.toCharArray()) {
			if (charMap.get(c) == null)
				charMap.put(c, 1);
			else
				return false;
		}
		return true;
	}
	
	public static boolean UniqueCharNoStructures(String sentence) {
		for (int i = 0; i < sentence.length()-1; i++) {
			for (int j = 1; j < sentence.length()-i; j++) {
				if (sentence.charAt(i) == sentence.charAt(i+j))
					return false;
			}
		}
		return true;
	}
	
	public static boolean IsPermut(String base, String s) {
		if (base.length() == s.length()) {
			for (char c : s.toCharArray()) {
				if (!base.contains(""+c)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	public static char[] CharReplace(char[] str) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < str.length; i++) {
			if (str[i] == ' ') {
				sb.append("%20");
			}
			else {
				sb.append(str[i]);
			}
		}
		return sb.toString().toCharArray();
	}
	
	public static String CompressString(String s) {
		StringBuffer sb = new StringBuffer();
		int cursor = 0;
		int repeats = 0;
		char c = 'a';
		while (cursor < s.length()) {
			if (s.charAt(cursor) == c) {
				repeats += 1;
			}
			else { 
				if (repeats != 0)
					sb.append(c).append(repeats);
				c = s.charAt(cursor);
				repeats = 1;
			}
			cursor++;
		}
		sb.append(c).append(repeats);
		String res = sb.toString();
		return (res.length() < s.length()) ? res : s;
	}
	
	public static int[][] RotateNotInPlace(int[][] image) {
		int len = image.length;
		int[][] result = new int[len][len];
		for (int i = 0; i < len; i++) {
			for (int j = 0; j < len; j++) {
				result[j][len-1-i] = image[i][j];
			}
		}
		return result;
	}
	
	public static void RotateInPlace(int[][] image) {
		int len = image.length;
		int placeholder;
		for (int i = 0; i < (len+1)/2; i++) {
			for (int j = 0; j < (len+1)/2; j++) {
				placeholder = image[i][j];
				image[i][j] = image[j][len-1-i];
				image[j][len-1-i] = image[len-1-i][len-1-j];
				image[len-1-i][len-1-j] = image[len-1-j][i];
				image[len-1-j][i] = placeholder;
						
			}
		}
	}
	
	public static void Nullify(int[][] matrix) {
		boolean[] rowsWithNull = new boolean[matrix.length];
		boolean[] colsWithNull = new boolean[matrix[0].length];
		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[0].length; col++) {
				if (matrix[row][col] == 0) {
					colsWithNull[col] = true;
					rowsWithNull[row] = true;
				}
			}
		}
		System.out.println(Arrays.toString(colsWithNull));
		System.out.println(Arrays.toString(rowsWithNull));
		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[0].length; col++) {
				if (rowsWithNull[row] || colsWithNull[col])
					matrix[row][col] = 0;
			}
		}
	}
	
	public static boolean IsRotation(String s1, String s2) {
		if (s1.length() == s2.length()) {
			return s2.concat(s2).indexOf(s1) != -1;
		}
		return false;
	}
	
	public static void main(String[] args) {
		/*
		 * UniqueChars
		 * 
		System.out.println(UniqueCharNoStructures("abacus"));
		System.out.println(UniqueCharWithDataStructures("abacus"));
		System.out.println(UniqueCharNoStructures("brandelog"));
		System.out.println(UniqueCharWithDataStructures("brandelog"));
		*/
		
		/*
		 * CsPermutation
		 * 
		System.out.println(IsPermut("greed", "heinousness"));
		System.out.println(IsPermut("ant", "tan"));
		System.out.println(IsPermut("greed", "greek"));
		*/
		
		/*
		 * CharReplace
		 * 
		System.out.println(CharReplace("I love Arachnids".toCharArray()));
		*/
		
		/*
		 * CompressString
		 * 
		System.out.println(CompressString("greed"));
		System.out.println(CompressString("boooooooooooolean"));
		*/
		
		/*
		 * RotateMatrix
		 * 
		int[][] rotated1 = RotateNotInPlace(new int[][] {{1,2},{3,4}});
		System.out.println(PrintMatrix(new int[][] {{1,2},{3,4}}));
		System.out.println(PrintMatrix(rotated1));
		int[][] rotated2 = (new int[][] {{1,2},{3,4}});
		RotateInPlace(rotated2);
		System.out.println(PrintMatrix(rotated2));
		*/
		
		/*
		 * Nullify
		 * 
		int[][] nullified = RotateNotInPlace(new int[][] {{1,2,3},{4,5,0},{7,8,9}});
		System.out.println(PrintMatrix(nullified));
		Nullify(nullified);
		System.out.println(PrintMatrix(nullified));
		*/
		
		/*
		 * IsRotation
		 * 
		System.out.println(IsRotation("greed", "reedg"));
		*/
	}
	
}
