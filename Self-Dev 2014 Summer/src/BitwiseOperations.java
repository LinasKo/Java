

public class BitwiseOperations {

	public static void main(String[] args) {
		int N = 0b1000111010101010101;
		int M = 0b101101;
		System.out.println(Integer.toBinaryString(N));
		System.out.println(Integer.toBinaryString(M));
		System.out.println(Integer.toBinaryString(insertBit(N, M, 3, 8)));
		System.out.println();
		
		int X = 0b101000101111101111110;
		int Z = 0b101000101111101111111;
		System.out.println(" x: " + Integer.toBinaryString(X));
		System.out.println("+x: " + Integer.toBinaryString(nextInt(X)));
		System.out.println(" z: " + Integer.toBinaryString(Z));
		System.out.println("+z: " + Integer.toBinaryString(nextInt(Z)));
		System.out.println();
		
		System.out.println(bitsToConvert(31, 14));
		System.out.println(bitsToConvert(16, 15));
		System.out.println(bitsToConvert(256, 15));
		System.out.println();
		
		int g = 0b101101010101110;
		System.out.println(Integer.toBinaryString(g));
		System.out.println(Integer.toBinaryString(swap(g)));
	}
	
	public static int insertBit(int N, int M, int i, int j) {
		M = M << i;
		int mask  = ~((1 << j-i+1)-1 << i);
		N = N & mask;
		N = N ^ M;
		return N;
	}
	
	public static boolean isOneAt(int N, int i) {
		return ((N & ~(1 << i)) != N);
	}
	
	public static int length(int x) {
		int len = 0;
		while(x != 0) {
			x = x >> 1;
			len++;
		}
		return len;
	}
	
	public static int nextInt(int N) {
		int zeroesBefore = 0;
		if (!isOneAt(N, 0)) {
			zeroesBefore++;
		}
		boolean flagStillZero = true;
		int index = 1;
		while (true) {
			if (isOneAt(N, index)){
				flagStillZero = false;
			}
			else if (flagStillZero) {
				zeroesBefore++;
			}
			else {
				zeroesBefore++;
				break;
			}
			index++;
		}
		N = N ^ (1 << index);
		for (int k1 = 1; k1 <= zeroesBefore; k1++) {
			N = N & ~(1 << index-k1);
		}
		for (int k2 = index-zeroesBefore-1; k2 >= 0; k2--) {
			N = N | (1 << k2);
		}
		return N;
	}
	
	public static int bitsToConvert(int a, int b) {
		int z = Math.abs(a-b);
		int bits = 0;
		int start = 2;
		while(z > 0) {
			if (start * 2 < z) {
				start *= 2;
			}
			else if (start > z) {
				start /= 2;
			}
			else {
				z -= start;
				bits += 1;
			}
		}
		return bits;
	}
	
	public static int swap(int x) {
		int len = length(x);
		int mask1 = 0;
		int mask2 = 0;
		for (int k = 0; k <= len/2+1; k++) {
			mask1 += (1 << k*2);
			mask2 += (1 << k*2+1);
		}
		mask1 &= x;
		mask2 &= x;
		mask1 = mask1 << 1;
		mask2 = mask2 >> 1;
		return (mask1 + mask2);
	}
}
