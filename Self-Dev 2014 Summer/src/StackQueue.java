import java.util.ArrayList;

public class StackQueue {
	public static void main(String[] args) {

		Stack st1 = CreateStack(new int[] { 7, 2, 3, 4, 5, 6, 1, 8, 9 });
		System.out.println(PrintOut(st1));
		st1.push(11);
		System.out.println(PrintOut(st1));
		System.out.println(st1.pop());
		System.out.println(PrintOut(st1));
		System.out.println(st1.getMin());
		System.out.println();

		SetOfStacks set1 = CreateSetOFStacks(new int[] { 7, 8, 8, 9, 6, 4, 3, 7, 8 });
		System.out.println(PrintOut(set1));
		System.out.println(set1.pop());
		System.out.println(PrintOut(set1));
		set1.push(17);
		set1.push(16);
		System.out.println(PrintOut(set1));
		System.out.println(set1.popAt(0));
		System.out.println(PrintOut(set1));
		System.out.println(set1.popAt(0));
		System.out.println(PrintOut(set1));
		System.out.println(set1.popAt(0));
		System.out.println(PrintOut(set1));
		System.out.println();

		QueueFromStacks qs = new QueueFromStacks();
		qs.enqueue(8);
		qs.enqueue(4);
		qs.enqueue(1);
		qs.enqueue(0);
		qs.dequeue(); // dequeue 8
		qs.dequeue(); // dequeue 4
		qs.enqueue(10);
		qs.enqueue(9); // Should be 1 0 10 9
		System.out.println(PrintOut(qs));
		System.out.println();
		
		AnimalShelter lese = new AnimalShelter();
		lese.enqueue(new Cat("Rusf"));
		lese.enqueue(new Dog("Dov"));
		lese.enqueue(new Dog("Zirgas"));
		lese.enqueue(new Cat("Narc"));
		lese.enqueue(new Dog("SUO"));
		lese.enqueue(new Cat("Visarionas"));
		System.out.println(lese.dequeueAny().name);
		System.out.println(lese.dequeueCat().name);
		System.out.println(lese.dequeueDog().name);
	}

	public static Stack CreateStack(int[] args) {
		Stack st = new Stack();
		for (int i : args) {
			st.push(i);
		}
		return st;
	}

	public static SetOfStacks CreateSetOFStacks(int[] args) {
		SetOfStacks set = new SetOfStacks();
		for (int i : args) {
			set.push(i);
		}
		return set;
	}

	public static String PrintOut(SetOfStacks s) {
		StringBuffer sb = new StringBuffer();
		Node n = s.GetLastStack().top;
		while (n != null) {
			sb.append(n.data + " ");
			n = n.next;
		}
		return sb.toString();
	}

	public static String PrintOut(QueueFromStacks s) {
		StringBuffer sb = new StringBuffer();
		Integer x = s.dequeue();
		while (x != null) {
			sb.append(x + " ");
			x = s.dequeue();
		}
		return sb.toString();
	}

	public static String PrintOut(Stack s) {
		StringBuffer sb = new StringBuffer();
		Node n = s.top;
		while (n != null) {
			sb.append(n.data + " ");
			n = n.next;
		}
		return sb.toString();
	}
}

class Stack {
	Node top = null;
	int min;

	public void push(int x) {
		if (top == null) {
			min = x;
		} else if (x < min) {
			min = x;
		}
		Node n = new Node(x);
		n.next = top;
		top = n;
	}

	public Integer pop() {
		if (top == null) {
			return null;
		} else {
			Integer res = top.data;
			top = top.next;
			return res;
		}
	}

	public int getMin() {
		return min;
	}
}

class SetOfStacks {
	ArrayList<Stack> stacks;
	static final int STACK_SIZE = 5;

	public SetOfStacks() {
		stacks = new ArrayList<Stack>();
		stacks.add(new Stack());
	}

	public Stack GetLastStack() {
		return stacks.get(stacks.size() - 1);
	}

	public Stack AddStack() {
		Stack st = new Stack();
		stacks.add(st);
		return st;
	}

	public int StackSize(Stack s) {
		int size = 0;
		Node n = s.top;
		while (n != null) {
			size++;
			n = n.next;
		}
		return size;
	}

	public void push(int x) {
		Stack last = GetLastStack();
		if (StackSize(last) == STACK_SIZE) {
			PushIfNew(x, AddStack(), last);
		} else {
			last.push(x);
		}
	}

	private void PushIfNew(int x, Stack st, Stack last) {
		Node n = new Node(x);
		n.next = last.top;
		st.top = n;
	}

	public Integer pop() {
		Stack st = GetLastStack();
		Integer x = st.pop();
		if (StackSize(st) == 0) {
			stacks.remove(stacks.size() - 1);
		}
		return x;
	}

	public Integer popAt(int i) {
		if (i < stacks.size()) {
			int x = stacks.get(i).pop();
			if (i != stacks.size() - 1) {
				Node n = stacks.get(i + 1).top;
				Node lastNode = null;
				for (int j = 0; j < STACK_SIZE; j++) { // Error here
					n = n.next;
				}
				lastNode = n;
				lastNode.next = stacks.get(i).top;
			}
			return x;
		} else
			return null;
	}
}

class QueueFromStacks {
	Stack primaryStack = new Stack();
	Stack returnStack = new Stack();

	public void enqueue(int x) {
		if (returnStack.top == null) {
			primaryStack.push(x);
		} else {
			Node n = returnStack.top;
			while (n != null) {
				primaryStack.push(returnStack.pop());
				n = returnStack.top;
			}
			primaryStack.push(x);
		}
	}

	public Integer dequeue() {
		Node n = primaryStack.top;
		while (n != null) {
			returnStack.push(primaryStack.pop());
			n = primaryStack.top;
		}
		if (returnStack.top == null) {
			return null;
		} else {
			Integer data = returnStack.top.data;
			returnStack.top = returnStack.top.next;
			return data;
		}
	}
}

abstract class Animal {
	String name;
	Animal next;
	
	Animal(String name) {
		this.name = name;
	}
}

class Cat extends Animal {
	String name;
	Cat next = null;

	Cat(String name) {
		super(name);
		this.name = name;
	}
}

class Dog extends Animal {
	String name;
	Dog next = null;

	Dog(String name) {
		super(name);
		this.name = name;
	}
}

class AnimalShelter {
	Animal firstDog = null;
	Animal firstCat = null;
	Animal lastDog, lastCat;
	static boolean IsLastDog;

	public void enqueue(Animal a) {
		if (a.getClass() == Dog.class) {
			if (firstDog == null) {
				firstDog = (Dog) a;
				lastDog = firstDog;
			} else {
				lastDog.next = (Dog) a;
				lastDog = lastDog.next;
			}
			IsLastDog = true;
		} else if (a.getClass() == Cat.class) {
			if (firstCat == null) {
				firstCat = (Cat) a;
				lastCat = firstCat;
			} else {
				lastCat.next = (Cat) a;
				lastCat = lastCat.next;
			}
			IsLastDog = false;
		}
	}

	public Animal dequeueAny() {
		if (firstCat == null && firstDog == null) {
			return null;
		} else {
			if (IsLastDog) {
				return dequeueDog();
			} else {
				return dequeueCat();
			}
		}
	}

	public Dog dequeueDog() {
		if (firstDog == null) {
			return null;
		} else {
			Dog dog = (Dog) firstDog;
			firstDog = firstDog.next;
			return dog;
		}
	}

	public Cat dequeueCat() {
		if (firstCat == null) {
			return null;
		} else {
			Cat cat = (Cat) firstCat;
			firstCat = firstCat.next;
			return cat;
		}
	}
}
