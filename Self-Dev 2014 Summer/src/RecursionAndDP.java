import java.util.ArrayList;
import java.util.Arrays;

public class RecursionAndDP {

	public static void main(String[] args) {
		System.out.println(stairs(20));
		System.out.println(robotPathCount(5, 7));
		System.out.println();

		Pair[] blockages = new Pair[] { new Pair(1, 2), new Pair(3, 4), new Pair(2, 2) };
		System.out.println(formIntArrayPrint(pathFind(5, 5, blockages)));

		for (int a : magicIndex(new int[] { 1, 2, 3, 4, 4, 6, 7, 7, 9, 11 })) {
			System.out.print(a + " ");
		}
		System.out.println("\n");

		allSsubsets(new Integer[] { 1, 2, 3, 3 });
		System.out.println(printArrSet(set));
		System.out.println();

		allPerms("", "GRuD".toCharArray());
		System.out.println(printArr(stringStorage));
		System.out.println();

		brackets(4, "");
		System.out.println(printArr(brack));
		System.out.println();

		System.out.println(moneyRepresentation(100));
		System.out.println();

		Integer[][] arr1 = new Integer[8][8];
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				arr1[i][j] = 0;
			}
		}
		queenPlacement(arr1, 0);
		/*
		for (Integer[][] grid1 : chessGrids) {
			for (Integer[] arr : grid1) {
				System.out.println(printArr(arr));
			}
			System.out.println();
		}
		*/
		System.out.println(chessGrids.size());
		System.out.println();
		
		

	}

	public static ArrayList<Integer[][]> chessGrids = new ArrayList<Integer[][]>();

	public static void queenPlacement(Integer[][] grid0, int row) {
		if (row == 8) {
			chessGrids.add(grid0);
			return;
		}
		for (int col = 0; col < 8; col++) {
			Integer[][] grid = new Integer[8][8];
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					grid[i][j] = grid0[i][j];
				}
			}
			boolean flag_ok = true;
			// Check whole column
			for (int row1 = 0; row1 < 8; row1++) {
				if (grid[row1][col] == 1) {
					flag_ok = false;
					break;
				}
			}
			if (flag_ok) {

				// Check diagonals
				for (int k = -7; k <= 7; k++) {
					if (row + k >= 0 && row + k <= 7 && col + k >= 0 && col + k <= 7) {
						if (grid[row + k][col + k] == 1) {
							flag_ok = false;
							break;
						}
					}
					if (row + k >= 0 && row + k <= 7 && col - k >= 0 && col - k <= 7) {
						if (grid[row + k][col - k] == 1) {
							flag_ok = false;
							break;

						}
					}
				}
				if (flag_ok) {
					grid[row][col] = 1;
					queenPlacement(grid, row + 1);
				}
			}
		}
	}

	public static int moneyRepresentation(int n) {
		int ways = 0;
		for (int _25 = 0; _25 * 25 <= n / 25; _25++) {
			for (int _10 = 0; _10 * 10 <= n - (_25 * 25); _10++) {
				for (int _5 = 0; _5 * 5 <= n - (_25 * 25 + _10 * 10); _5++) {
					ways++;
				}
			}
		}
		return ways;
	}

	public static ArrayList<String> brack = new ArrayList<String>();

	public static void brackets(int n, String s) {
		if (n == 0) {
			if (!brack.contains(s))
				brack.add(s);
		} else {
			brackets(n - 1, "()" + s);
			brackets(n - 1, s + "()");
			brackets(n - 1, "(" + s + ")");
		}
	}

	public static ArrayList<String> stringStorage = new ArrayList<String>();

	public static void allPerms(String s, char[] freeChars) {
		if (freeChars.length == 0 && !stringStorage.contains(s)) {
			stringStorage.add(s);
			return;
		}
		for (int i = 0; i < freeChars.length; i++) {
			allPerms(s + freeChars[i], removeAtIndex(freeChars, i));
		}
	}

	public static String printArrSet(ArrayList<Integer[]> s) {
		StringBuffer sb = new StringBuffer();
		for (Integer[] arr : s) {
			sb.append(printArr(arr) + "\n");
		}
		return sb.toString();
	}

	public static String printArr(Integer[] arr) {
		StringBuffer sb = new StringBuffer();
		for (int i : arr) {
			sb.append(i + " ");
		}
		return sb.toString();
	}

	public static String printArr(ArrayList<String> arr) {
		StringBuffer sb = new StringBuffer();
		for (String i : arr) {
			sb.append(i + " ");
		}
		return sb.toString();
	}

	public static ArrayList<Integer[]> set = new ArrayList<Integer[]>();

	public static void allSsubsets(Integer[] s) {
		Arrays.sort(s);
		boolean flag_ok = true;
		for (Integer[] entry : set) {
			if (Arrays.equals(entry, s)) {
				flag_ok = false;
				break;
			}
		}
		if (flag_ok) {
			set.add(s);
		}
		for (int i = 0; i < s.length; i++) {
			allSsubsets(removeAtIndex(s, i));
		}
	}

	public static Integer[] removeAtIndex(Integer[] arr, int index) {
		Integer[] res = new Integer[arr.length - 1];
		for (int i = 0; i < arr.length; i++) {
			if (i < index) {
				res[i] = arr[i];
			} else if (i > index) {
				res[i - 1] = arr[i];
			}
		}
		return res;
	}

	public static char[] removeAtIndex(char[] arr, int index) {
		char[] res = new char[arr.length - 1];
		for (int i = 0; i < arr.length; i++) {
			if (i < index) {
				res[i] = arr[i];
			} else if (i > index) {
				res[i - 1] = arr[i];
			}
		}
		return res;
	}

	public static String formIntArrayPrint(int[][] a) {
		StringBuffer sb = new StringBuffer();
		for (int[] row : a) {
			for (int val : row) {
				sb.append(val + " ");
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	public static int stairs(int n) {
		int[] ways = new int[n];
		ways[0] = 1;
		for (int i = 0; i < n - 1; i++) {
			ways[i + 1] += ways[i];
			if (i + 2 < n) {
				ways[i + 2] += ways[i];
			}
			if (i + 3 < n) {
				ways[i + 3] += ways[i];
			}
		}
		return ways[n - 1];
	}

	public static int robotPathCount(int x, int y) {
		if (x < 0 || y < 0) {
			return -1;
		} else if (x == 0 && y == 0) {
			return 0;
		} else if (x == 1 || y == 1) {
			return 1;
		} else {
			int paths = 0;
			if (x != 0) {
				paths += robotPathCount(x - 1, y);
			}
			if (y != 0) {
				paths += robotPathCount(x, y - 1);
			}
			return paths;
		}
	}

	public static void clearPath(int[][] board) {
		for (int y = 0; y < board.length - 1; y++) {
			for (int x = 0; x < board[0].length - 1; x++) {
				if (board[y][x] != -1) {
					if (board[y + 1][x] == board[y][x + 1]) {
						board[y + 1][x] = 0;
					}
					int num = board[y][x];
					if (y > 0 && x > 0) {
						if (board[y - 1][x] != num - 1 && board[y][x - 1] != num - 1) {
							board[y][x] = 0;
						}
					} else if (y > 0 && board[y - 1][x] != num - 1) {
						board[y][x] = 0;
					} else if (x > 0 && board[y][x - 1] != num - 1) {
						board[y][x] = 0;
					}
				}
			}
		}
	}

	public static int[][] pathFind(int x_size, int y_size, Pair[] blockages) {
		int[][] board = new int[y_size][x_size];
		for (Pair coords : blockages) {
			board[coords.y][coords.x] = -1;
		}
		board[y_size - 1][x_size - 1] = x_size + y_size - 1;
		for (int row = y_size - 1; row >= 0; row--) {
			for (int col = x_size - 1; col >= 0; col--) {
				if (board[row][col] != 0 && board[row][col] != -1) {
					if (row >= 1 && board[row - 1][col] != -1) {
						board[row - 1][col] = board[row][col] - 1;
					}
					if (col >= 1 && board[row][col - 1] != -1) {
						board[row][col - 1] = board[row][col] - 1;
					}
				}
			}
		}
		clearPath(board);
		return board;
	}

	public static ArrayList<Integer> magicIndex(int[] A) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		for (int i = 0; i < A.length; i++) {
			if (A[i] == i) {
				res.add(i);
			}
		}
		return res;
	}
}

class Pair {
	int x, y;

	public Pair(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
