public class Node {
	int data;
	Node next = null;
	
	public Node(int data) {
		this.data = data;
	}
	
	public static Node AppendToBack(Node chain, Node x) {
		Node head = chain;
		while (chain.next != null) {
			chain = chain.next;
		}
		chain.next = x;
		return head;
	}
	
	public static Node MakeList(int[] content) {
		Node n = new Node(content[0]);
		for (int i = 1; i < content.length; i++) {
			AppendToBack(n, new Node(content[i]));
		}
		return n;
	}
	
	public static String PrintOut(Node n) {
		StringBuffer sb = new StringBuffer();
		while (n.next != null) {
			sb.append(n.data + " ");
			n = n.next;
		}
		sb.append(n.data + " ");
		return sb.toString();
	}
	
	public static Node GetNode(Node n, int index) {
		for (int i = 0; i < index; i++) {
			n = n.next;
		}
		return n;
	}
}
