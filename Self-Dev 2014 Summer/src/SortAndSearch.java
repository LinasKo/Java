import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class SortAndSearch {
	
	public static void main(String[] args) {
		int[] A = new int[11];
		int[] aux_A = {1,2,4,8,9};
		for (int i = 0; i < 5; i++) {
			A[i] = aux_A[i];
		}
		int[] B = new int[]{0,3,4,7,9,10};
		mergeTwo(A, B);
		System.out.println(printArr(A));
		System.out.println();
		
		String[] anagrams = new String[]{"greed", "cat", "tac", "atc", "reedg", "kraken", "tca", "torus"};
		sortAnagrams(anagrams);
		System.out.println(printArr(anagrams));
		System.out.println();
		
		int[] searchable = new int[]{12,17,19,23,25,49,5,9,10,11,11};
		System.out.println(searchInRotated(searchable, 23));
		System.out.println(searchInRotated(searchable, 9));
		System.out.println();
		
		System.out.println(findString(new String[]{"", "", "car", "", "", "", "Waldo", ""}, "Waldo"));
		System.out.println();
	}
	
	public static int findString(String[] s, String value) {
		for (int i = 0; i < s.length; i++) {
			if (s[i] == value) {
				return i;
			}
		}
		return -1; // Error
	}
	
	public static int searchInRotated(int[] A, int value) {
		int high;
		int i = 0;
		while (true) {
			if (i == A.length-1) {
				high = A.length;
			}
			else if (A[i+1] < A[i]) {
				high = i;
				break;
			}
			i++;
		}
		if (value >= A[0]) {
			return binarySearch(Arrays.copyOfRange(A, 0, high+1), value);
		}
		else {
			return high+1 + binarySearch(Arrays.copyOfRange(A, high+1, A.length), value);
		}
	}
	
	public static int binarySearch(int[] A, int value) {
		int n = A[A.length/2];
		if (n == value) {
			return A.length/2;
		}
		else if (value > n) {
			return A.length/2 + binarySearch(Arrays.copyOfRange(A, A.length/2+1, A.length), value);
		}
		else {
			return binarySearch(Arrays.copyOfRange(A, 0, A.length/2), value);
		}
	}
	
	public static void sortAnagrams(String[] A) {
		HashMap<String, ArrayList<String>> strs = new HashMap<>();
		for (String s : A) {
			char[] s_sorted = s.toCharArray();
			Arrays.sort(s_sorted);
			String new_s = new String(s_sorted);
			if (!strs.containsKey(new_s)) {
				strs.put(new_s, new ArrayList<String>());
			}
			strs.get(new_s).add(s);
		}
		ArrayList<String> res = new ArrayList<String>();
		for (String key : strs.keySet()) {
				res.addAll(strs.get(key));
		}
		A = res.toArray(new String[res.size()]);
	}
	
	public static String printArr(int[] A) {
		StringBuffer sb = new StringBuffer();
		for (int a : A) {
			sb.append(a + " ");
		}
		return sb.toString();
	}
	
	public static String printArr(String[] A) {
		StringBuffer sb = new StringBuffer();
		for (String a : A) {
			sb.append(a + " ");
		}
		return sb.toString();
	}
	
	public static void mergeTwo(int[] A, int[] B) {
		int A_index = 0;
		for (int j = 0; j < B.length; j++) {
			if (A_index >= A.length) {
				break;
			}
			else if (A[A_index] > B[j]) {
				push(A, A_index);
				A[A_index] = B[j];
			}
			else if (A[A_index] > A[A_index+1]) {
				for (int k = j; k < B.length; k++) {
					A_index++;
					A[A_index] = B[k];
				}
				break;
			}
			else {
				j--;
			}
			A_index++;
		}
	}
	
	private static void push(int[] A, int index) {
		for (int i = A.length-1; i > index; i--) {
			A[i] = A[i-1];
		}
	}
}
