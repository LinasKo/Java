import java.util.ArrayList;



class Tree {
	NodeD root;
	
	public Tree(int data) {
		root = new NodeD(data);
		root.children = new ArrayList<NodeD>();
	}
}