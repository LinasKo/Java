package max_flows;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class MaxFlow2 {

	final static int VERTEX_N = 7;
	static int[][] cap = new int[VERTEX_N][VERTEX_N];
	
	final static int SOURCE = 0;
	final static int SINK = 6;

	public static void main(String[] args) {
		
		// Init
		cap[SOURCE][1] = 11;
		cap[SOURCE][3] = 5;
		cap[1][5] = 5;
		cap[1][2] = 7;
		cap[2][4] = 9;
		cap[3][2] = 2;
		cap[3][4] = 2;
		cap[4][SINK] = 12;
		cap[5][2] = 3;
		cap[5][SINK] = 2;
		
		PrintStuff.printMatrix(cap);
		
		int path = 1;
		int sum = 0;
		while (path != 0) {
			path = bfs();
			sum += path;
		}
		System.out.println(sum);
		
		PrintStuff.printMatrix(cap);
	}

	public static int bfs() {
		Queue<Integer> q = new LinkedList<Integer>();
		boolean[] visited = new boolean[VERTEX_N];
		q.add(0);
		visited[0] = true;
		 // find shortest path, e.g. form a 'from' array
		int[] from = new int[VERTEX_N];
		Arrays.fill(from, -1);
		int where;
		outer_loop: while (!q.isEmpty()) {
			where = q.poll();
			for (int next = 0; next < VERTEX_N; next++) {
				if (!visited[next] && cap[where][next] > 0) {
					q.add(next);
					visited[next] = true;
					from[next] = where;
					if (next == SINK) {
						break outer_loop;
					}
				}
			}
		}
		
		// Find path capacity
		where = SINK;
		int path_cap = Integer.MAX_VALUE;
		while (from[where] != -1) {
			int prev = from[where];
			path_cap = Math.min(path_cap, cap[prev][where]);
			where = prev; // Don't forget the loop iterator!
		}
		
		// Update the network
		where = SINK;
		while (from[where] != -1) {
			int prev = from[where];
			cap[prev][where] -= path_cap;
			cap[where][prev] += path_cap;
			where = prev;
		}
		if (path_cap == Integer.MAX_VALUE) {
			return 0;
		}
		return path_cap;
	}
}
