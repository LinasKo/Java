package max_flows;

import java.util.LinkedList;
import java.util.Queue;

class Vertex {
	Vertex[] adjacent;
	int num;

	public Vertex(int num) {
		this.num = num;
	}

	public void setAdjacent(Vertex[] adjacent) {
		this.adjacent = adjacent;
	}
}

public class MaxFlow1 {

	static int[][] cap = new int[6][6];

	public static void main(String[] args) {

		// Init
		cap[0][1] = 4;
		cap[0][2] = 5;
		cap[0][3] = 4;
		cap[1][4] = 4;
		cap[2][4] = 6;
		cap[3][5] = 5;
		cap[4][5] = 5;

		Vertex sink = new Vertex(5);
		Vertex source = new Vertex(0);
		Vertex v1 = new Vertex(1);
		Vertex v2 = new Vertex(2);
		Vertex v3 = new Vertex(3);
		Vertex v4 = new Vertex(4);
		sink.setAdjacent(new Vertex[] { v3, v4 });
		source.setAdjacent(new Vertex[] { v1, v2, v3 });
		v1.setAdjacent(new Vertex[] { source, v4 });
		v2.setAdjacent(new Vertex[] { source, v4 });
		v3.setAdjacent(new Vertex[] { source, sink });
		v4.setAdjacent(new Vertex[] { v1, v2, sink });

		int path_length = -1;
		int sum = 0;
		while (path_length != 0) {
			path_length = bfs(source, sink);
			sum += path_length;
		}
		
		System.out.println(sum);
		
		
	}

	public static int bfs(Vertex source, Vertex sink) {
		// Find the shortest augmenting path
		// -> Build a 'from' array
		Queue<Vertex> q = new LinkedList<Vertex>();
		boolean[] visited = new boolean[6];
		q.add(source);
		visited[source.num] = true;
		Vertex[] from = new Vertex[6];
		Vertex where;
		outer_loop: while (!q.isEmpty()) {
			where = q.poll();
			for (Vertex next : where.adjacent) {
				if (!visited[next.num] && cap[where.num][next.num] > 0) {
					q.add(next);
					visited[next.num] = true;
					from[next.num] = where;
					if (next == sink) {
						break outer_loop;
					}
				}
			}
		}

		// Get the path capacity
		// -> via variable 'path_cap'
		where = sink;
		int path_cap = Integer.MAX_VALUE;
		while (from[where.num] != null) {
			Vertex prev = from[where.num];
			path_cap = Math.min(path_cap, cap[prev.num][where.num]);
			where = prev;
		}

		// Augment the network
		where = sink;
		while (from[where.num] != null) {
			Vertex prev = from[where.num];
			cap[prev.num][where.num] -= path_cap;
			cap[where.num][prev.num] += path_cap;
			where = prev;
		}
		if (path_cap == Integer.MAX_VALUE) {
			return 0;
		}
		return path_cap;
	}

}
