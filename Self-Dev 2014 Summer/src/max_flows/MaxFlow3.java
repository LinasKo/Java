package max_flows;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class MaxFlow3 {

	// Init
	final static int N = 10+5;
	static int[][] cap = new int[N][N];
	final static int SOURCE = 0;
	final static int SINK = 9;
	
	public static void main(String[] args) {
		
		cap[0][2] = 8;
		cap[0][1] = 9;
		cap[1][2] = 4;
		cap[1][12] = Integer.MAX_VALUE;
		cap[2][3] = 1;
		cap[2][4] = 8;
		cap[3][4] = 4;
		cap[3][5] = 7;
		cap[3][7] = 2;
		cap[4][10] = Integer.MAX_VALUE;
		cap[4][5] = 5;
		cap[5][13] = Integer.MAX_VALUE;
		cap[5][6] = 9;
		cap[5][9] = 12;
		cap[6][8] = 8;
		cap[6][7] = 2;
		cap[7][14] = Integer.MAX_VALUE;
		cap[7][1] = 5;
		cap[7][8] = 3;
		cap[8][11] = Integer.MAX_VALUE;
		cap[8][9] = 10;
		cap[10][2] = 8;
		cap[11][6] = 8;
		cap[12][7] = 5;
		cap[13][3] = 7;
		cap[14][3] = 2;
		
		PrintStuff.printMatrix(cap);
		
		int i = 1;
		int sum = 0;
		while(i != 0) {
			i = bfs();
			sum += i;
		}
		System.out.println(sum);
		
		PrintStuff.printMatrix(cap);
	}
	
	public static int bfs() {
		boolean[] visited = new boolean[N];
		Queue<Integer> q = new LinkedList<Integer>();
		q.add(SOURCE);
		visited[SOURCE] = true;
		int[] from = new int[N];
		Arrays.fill(from, -1);
		int where;
		outer_loop: while (!q.isEmpty()) {
			where = q.poll();
			for (int next = 0; next < N; next++) {
				if (!visited[next] && cap[where][next] > 0) {
					q.add(next);
					visited[next] = true;
					from[next] = where;
					if (next == SINK) {
						break outer_loop;
					}
				}
			}
		}
		where = SINK;
		int path_cap = Integer.MAX_VALUE;
		while (from[where] != -1) {
			int prev = from[where];
			path_cap = Math.min(path_cap, cap[prev][where]);
			where = prev;
		}
		where = SINK;
		while (from[where] != -1) {
			int prev = from[where];
			cap[prev][where] -= path_cap;
			cap[where][prev] += path_cap;
			where = prev;
		}
		if (path_cap == Integer.MAX_VALUE) {
			return 0;
		}
		return path_cap;
		
	}
	
}
