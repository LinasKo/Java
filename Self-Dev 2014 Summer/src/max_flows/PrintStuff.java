package max_flows;

public class PrintStuff {

	public static void printMatrix(int[][] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j : a[i]) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
}
