package max_flows;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class MaxFloat4 {

	public static final int SOURCE = 0;
	public static final int SINK = 5;

	static final int N = 6;
	static final int[][] cap = new int[N][N];

	public static void main(String[] args) {

		cap[0][1] = 5;
		cap[0][2] = 5;
		cap[1][5] = 4;
		cap[2][3] = 2;
		cap[2][4] = 2;
		cap[3][5] = 2;
		cap[3][4] = 9;
		cap[4][5] = 4;

		int i = 1, sum = 0;
		while (i != 0) {
			i = bfs();
			sum += i;
		}
		System.out.println(sum);
	}

	public static int bfs() {
		boolean[] visited = new boolean[N];
		Queue<Integer> q = new LinkedList<Integer>();
		q.add(SOURCE);
		visited[SOURCE] = true;
		int[] from = new int[N];
		Arrays.fill(from, -1);
		int where;
		outer_loop: while (!q.isEmpty()) {
			where = q.poll();
			for (int next = 0; next < N; next++) {
				if (cap[where][next] > 0 && !visited[next]) {
					q.add(next);
					visited[next] = true;
					from[next] = where;
					if (next == SINK) {
						break outer_loop;
					}
				}
			}
		}
		where = SINK;
		int path_cap = Integer.MAX_VALUE;
		while (from[where] != -1) {
			int prev = from[where];
			path_cap = Math.min(path_cap, cap[prev][where]);
			where = prev;
		}
		where = SINK;
		while (from[where] != -1) {
			int prev = from[where];
			cap[prev][where] -= path_cap;
			cap[where][prev] += path_cap;
			where = prev;
		}
		if (path_cap == Integer.MAX_VALUE) {
			return 0;
		}
		return path_cap;
	}
}
