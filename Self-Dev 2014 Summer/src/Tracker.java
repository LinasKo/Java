import java.util.ArrayList;
import java.util.HashMap;


public class Tracker {

	private static ArrayList<Integer> list = new ArrayList<>();
	private static HashMap<Integer, Integer> numbers = new HashMap<>();
	private static int highest;
	private static int lowest;
	
	public static void main(String[] args) {
		track(5);
		track(7);
		track(9);
		track(5);
		track(7);
		track(1);
		track(5);
		System.out.println(getRankOfNumber(7));
	}
	
	public static void track(int x) {
		list.add(x);
		if (x < lowest) {
			lowest = x;
		}
		if (x > highest) {
			highest = x;
		}
		if (numbers.keySet().contains(x)) {
			numbers.put(x, numbers.get(x)+1);
		}
		else {
			numbers.put(x, 1);
		}
	}
	
	public static int getRankOfNumber(int x) {
		int rank = -1;
		if (numbers.containsKey(x)) {
			for (Integer key : numbers.keySet()) {
				if (key <= x) {
					rank += numbers.get(key);
				}
			}
		}
		return rank;
	}
}
