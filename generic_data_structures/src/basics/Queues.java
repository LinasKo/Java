package basics;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;


/**
 * Created by dekondra on 23/04/17.
 */
public class Queues {
    public static void main(String[] args) {
        Queue<String> q = new LinkedList<>();

        for (int i = 0; i < 15; i++) {
            q.add(Integer.toString(i));
        }

        System.out.println(q.poll());
        System.out.println(q.poll());

        System.out.println(q.offer("fsd"));


        Queue<Integer> p = new PriorityQueue<>();
        Random rand = new Random(743);

        System.out.print("[ ");
        for (int i = 0; i < 15; i++) {
            int r = rand.nextInt(15);
            p.offer(r);
            System.out.print(r + " ");
        }
        System.out.println("]");

        System.out.print("[ ");
        while (!p.isEmpty()) {
            int r = p.poll();
            System.out.print(r + " ");
        }
        System.out.println("]");
    }
}
