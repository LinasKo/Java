package basics;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;


/**
 * Created by dekondra on 23/04/17.
 */
public class Sets {
    public static void main(String[] args) {
        Set<String> s = new HashSet<>();
        Random rand = new Random();

        for (int i = 0; i < 15; i++) {
            s.add(Integer.toString(rand.nextInt(10)));
        }

        for (String str : s) {
            System.out.print(str + " ");
        }
    }
}
