package basics;

/**
 * Created by dekondra on 22/04/17.
 */
public class Arrays {
    public static void main(String args[]) {
        int[] a = new int[15];

        for (int i = 0; i < a.length; i++) {
            int k = (i+5) % a.length;
            a[i] = k;
            System.out.println(k);
        }

        System.out.print("[ ");
        for (int z : a) {
            System.out.print(z + " ");
        }
        System.out.println("]");
    }
}
