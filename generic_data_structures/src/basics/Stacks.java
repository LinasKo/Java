package basics;

import java.util.Stack;

/**
 * Created by dekondra on 22/04/17.
 */
public class Stacks {
    public static void main(String[] args) {
        Stack<String> stack = new Stack<>();

        // System.out.println(stack.peek()); --> crashes

        for (int i = 0; i < 15; i++) {
            stack.push(Integer.toString(i));
        }

        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack.peek());

        System.out.println(stack.search("12"));

        System.out.println(stack.empty());
    }
}
