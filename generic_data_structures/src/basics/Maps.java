package basics;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;


/**
 * Created by dekondra on 22/04/17.
 */
public class Maps {
    public static void main(String args[]) {
        Map<String, Integer> map = new HashMap<>();
        Random random = new Random(743);

        for (int i = 0; i < 15; i++) {
            int randInt = random.nextInt(15);
            map.put(Integer.toString(i), randInt);
        }

        System.out.print("[ ");
        for (String key : map.keySet()) {
            System.out.print(key + ":" + map.get(key) + " ");
        }
        System.out.println("]");

        // BEGIN

        map.remove("7");
        System.out.println(map.containsKey("8"));

        // END

        System.out.print("[ ");
        for (String key : map.keySet()) {
            System.out.print(key + ":" + map.get(key) + " ");
        }
        System.out.println("]");
    }
}
