package basics;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;

/**
 * Created by dekondra on 22/04/17.
 */
public class Lists {
    public static void main(String[] args) {
        Random rand = new Random(743);
        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < 15; i++) {
            int randNum = rand.nextInt(15);
            numbers.add(randNum);
        }

        System.out.print("[ ");
        for (int z : numbers) {
            System.out.print(z + " ");
        }
        System.out.println("]");


        Collections.sort(numbers);

        System.out.print("[ ");
        for (int z : numbers) {
            System.out.print(z + " ");
        }
        System.out.println("]");

        numbers.remove(new Integer(5));
        numbers.remove(5);
        numbers.add(0, -999);

        System.out.print("[ ");
        for (int z : numbers) {
            System.out.print(z + " ");
        }
        System.out.println("]");
    }
}
