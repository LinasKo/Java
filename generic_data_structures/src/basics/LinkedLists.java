package basics;

import java.util.LinkedList;


/**
 * Created by dekondra on 22/04/17.
 */
public class LinkedLists {
    public static void main(String[] args) {
        LinkedList<String> linkedList = new LinkedList<>();

        System.out.println(linkedList.peek());
        // System.out.println(linkedList.element()); --> crashes
        // System.out.println(linkedList.getFirst()); --> crashes

        for (int i = 0; i < 15; i++) {
            linkedList.add(Integer.toString(i));
        }

        System.out.println(linkedList.getFirst());
        System.out.println(linkedList.getFirst());

        System.out.println(linkedList.element());
        System.out.println(linkedList.element());

        System.out.println(linkedList.getLast());
        System.out.println(linkedList.getLast());

        System.out.println(linkedList.offer("Offer"));
        System.out.println(linkedList.offerFirst("OfferFirst"));
        System.out.println(linkedList.offerLast("OfferLast"));

        System.out.println(linkedList.pop());
        System.out.println(linkedList.poll());
        System.out.println(linkedList.pollLast());

        linkedList.push("Muahaha");

        System.out.print("[ ");
        for (String elem : linkedList) {
            System.out.print(elem + " ");
        }
        System.out.println("]");
    }
}
