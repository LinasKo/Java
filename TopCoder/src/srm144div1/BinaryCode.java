package srm144div1;

public class BinaryCode {
	
	public static String[] decode(String message) {
		if (message == "0") {
			return new String[]{"0", "0"};
		}
		else if (message == "1") {
			return new String[]{"1", "1"};
		}
		else if (message.length() <= 1) {
			return new String[]{"NONE", "NONE"};
		}
		String[] res = new String[2];
		StringBuilder sb = new StringBuilder();
		int[] mess = new int[message.length()];
		for (int c = 0; c < message.length(); c++) {
			mess[c] = Integer.parseInt(message.substring(c, c+1));
		}
		
		int[] s1 = new int[message.length()];
		s1[0] = 0;
		s1[1] = mess[0] - s1[0];
		
		for (int i = 2; i < message.length(); i++) {
			s1[i] = mess[i-1] - s1[i-1] - s1[i-2];
		}
		if (mess[message.length()-1] != s1[message.length()-2] + s1[message.length()-1]) {
			s1 = new int[]{2};
		}
		boolean flag = true;
		for (int i : s1) {
			if (!(i==1 || i==0)) {
				res[0] = "NONE";
				flag = false;
				break;
			}
		}
		if (flag) {
			for (int i : s1) {
				sb.append(i);
			}
			res[0] = sb.toString();
			sb.delete(0, sb.length());
		}
		
		int[] s2 = new int[message.length()];
		s2[0] = 1;
		s2[1] = mess[0] - s2[0];
		
		for (int i = 2; i < message.length(); i++) {
			s2[i] = mess[i-1] - s2[i-1] - s2[i-2];
		}
		if (mess[message.length()-1] != s2[message.length()-2] + s2[message.length()-1]) {
			s2 = new int[]{2};
		}
		flag = true;
		for (int i : s2) {
			if (!(i==1 || i==0)) {
				res[1] = "NONE";
				flag = false;
				break;
			}
		}
		if (flag) {
			for (int i : s2) {
				sb.append(i);
			}
			res[1] = sb.toString();
		}
		

		return res;
	}
}
