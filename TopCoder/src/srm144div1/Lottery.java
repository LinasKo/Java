package srm144div1;

class Game {

	public String name;
	public int choices;
	public int blanks;
	public boolean sorted;
	public boolean unique;

	public Game(String input) {
		setup(input);
	}

	public void setup(String input) {
		String[] sts1 = input.split(":");
		name = sts1[0];
		String[] sts2 = sts1[1].split(" ");
		choices = Integer.parseInt(sts2[1]);
		blanks = Integer.parseInt(sts2[2]);
		sorted = sts2[3].equals("T") ? true : false;
		unique = sts2[4].equals("T") ? true : false;
	}

	public String toString() {
		return String.format("%s: %s %s %s %s", name, choices, blanks, sorted, unique);
	}

	public long validTickets() {
		if (sorted) {
			if (!unique) {
				return choose(blanks, choices + blanks - 1);
			} else {
				return choose(blanks, choices);
			}
		}
		else if (!unique) {
			return (long) Math.pow(choices, blanks);
		} else {
			long prod = 1;
			for (int i = choices; i > choices - blanks; i--) {
				prod *= i;
			}
			return prod;
		}
	}

	private long choose(int blanks, int choices) {
		long res = 1; 
		for (int i = choices; i > choices - blanks; i--) {
			res *= i;
		}
		for (int i = blanks; i > 0; i--) {
			res /= i;
		}
		return res;
	}
}

public class Lottery {

	public static String[] sortByOdds(String[] rules) {
		String[] names = new String[rules.length];
		long[] values = new long[rules.length];
		for (int i = 0; i < rules.length; i++) {
			Game g = new Game(rules[i]);
			names[i] = g.name;
			values[i] = g.validTickets();
		}
		for (int i = 0; i < names.length-1; i++) {
			for (int j = 0; j < names.length-1-i; j++) {
				if (values[j] > values[j+1]) {
					long tmp_val = values[j];
					values[j] = values[j+1];
					values[j+1] = tmp_val;
					String tmp_str = names[j];
					names[j] = names[j+1];
					names[j+1] = tmp_str;
				}
				if (values[j] == values[j+1]) {
					if (names[j].compareTo(names[j+1]) > 0) {
						long tmp_val = values[j];
						values[j] = values[j+1];
						values[j+1] = tmp_val;
						String tmp_str = names[j];
						names[j] = names[j+1];
						names[j+1] = tmp_str;
					}
				}
			}
		}
		return names;
	}
}
