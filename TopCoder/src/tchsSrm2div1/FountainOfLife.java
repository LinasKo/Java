package tchsSrm2div1;

public class FountainOfLife {

	public static double elixirOfDeath(int elixir, int poison, int pool) {
		double res = (double) pool / (poison - elixir);
		if (res < 0) {
			return -1.0;
		}
		else {
			return res;
		}
	}
}
