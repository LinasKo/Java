package tchsSrm1;

public class SpeedRadar {
	
	public static double averageSpeed(int minLimit, int maxLimit, int[] readings) {
		int infringements = 0;
		int total = 0;
		double sum = 0;
		for (int i : readings) {
			if (i < minLimit || i > maxLimit) {
				infringements++;
			}
			else {
				sum += i;
				total++;
			}
		}
		if ((double) infringements/(total+infringements) > 0.1) {
			return 0.0;
		}
		else {
			return sum / total;
		}
	}
}
