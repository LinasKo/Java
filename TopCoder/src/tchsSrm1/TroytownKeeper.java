package tchsSrm1;

public class TroytownKeeper {

	public static int limeLiters(String[] maze) {
		int[][] map = new int[maze.length + 2][maze[0].length() + 2];
		boolean changed = true;
		// Flood fill
		while (changed) {
			changed = false;
			for (int i = 0; i <= maze.length + 1; i++) {
				for (int j = 0; j <= maze[0].length() + 1; j++) {
					// Flood the perimeter
					if (i == 0 || j == 0 || i == maze.length + 1 || j == maze[0].length() + 1) {
						if (map[i][j] == 0) {
							map[i][j] = 1;
							changed = true;
						}
					}
					// Flood the insides
					else if (maze[i - 1].charAt(j - 1) == '.' && map[i][j] == 0) {
						if (map[i + 1][j] == 1 || map[i][j + 1] == 1 || map[i - 1][j] == 1 || map[i][j - 1] == 1) {
							map[i][j] = 1;
							changed = true;
						}
					}
				}
			}
		}
		// Calculate wall number
		int walls = 0;
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				if (map[i][j] == 1) {
					if (map[(i + 1) % map.length][j] == 0) {
						walls++;
					}
					if (map[i][(j + 1) % map[0].length] == 0) {
						walls++;
					}
					if (map[(i - 1  + map.length) % map.length][j] == 0) {
						walls++;
					}
					if (map[i][(j - 1 + map[0].length) % map[0].length] == 0) {
						walls++;
					}
				}
			}
		}
		return walls;
	}

	public static void main(String[] args) {
		String[] s1 = new String[] { "##..#", "#.#.#", "#.#.#", "#####" };
		System.out.println(limeLiters(s1));
	}
}
