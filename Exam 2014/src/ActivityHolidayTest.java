import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class ActivityHolidayTest {
	
	ActivityHoliday a;

	@Before
	public void setUp()  {
		a = new ActivityHoliday("Glasgow", 14);
	}

	@Test
	public void testToString() {
		String s = a.toString();
	}

	@Test
	public void testAddActivity() {
		// Passing null for the Cost, so that if the student's Cost code is all
		// wrong but their ActivityHoliday code is OK, we don't make this test
		// fail - only their Cost tests should fail in that case.
		a.addActivity("Hunterian Museum", null);
	}
	
	@Test
	public void testCorrectOutput() {
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("Glasgow (14)\n");
		strBuff.append("Activities:\n");
		strBuff.append("Mackintosh House 5 pounds sterling\n");
		strBuff.append("Hunterian Museum 5 pounds sterling");
		String s = strBuff.toString();
		
		Cost c = new Cost();
		c.setAmount(5);
		c.setCurrency("pounds sterling");
		a.addActivity("Hunterian Museum", c);
		a.addActivity("Mackintosh House", c);
		assertEquals(a.toString(), s);
	}

}