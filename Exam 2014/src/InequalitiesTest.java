import static org.junit.Assert.*;

import org.junit.Test;


public class InequalitiesTest {

	@Test
	public void testDotProduct() {
		int i = Inequalities.dotProduct(new int[] { 2, 1 }, new int[] { 3, 4 });
		assertEquals(10, i);
		int j = Inequalities.dotProduct(new int[] {2, 1, 3}, new int[] {3, 4});
		assertEquals(0, j);

	}

	@Test
	public void testCS() {
		boolean b = Inequalities.cs(new int[] { 2, 1 }, new int[] { 3, 4 });
		assertEquals(true, b);
	}

	@Test
	public void testConcatenate() {
		int[] i = Inequalities.concatenate(new int[] { 2, 1 }, new int[] { 3, 4, 5 });
		assertArrayEquals(new int[] {2, 1, 3, 4, 5}, i);
	}

	@Test
	public void testAMGM() {
		boolean b = Inequalities.amgm(new int[] { 4, 9 });
		assertEquals(true, b);
	}

	@Test
	public void testMain() {
		Inequalities.main(new String[] { "2", "2", "1", "3", "4" });
	}

}