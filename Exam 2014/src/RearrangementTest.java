import static org.junit.Assert.*;

import org.junit.Test;


public class RearrangementTest {

	@Test
	public void testDotWith() {
		int i = Rearrangement.dotWith(new int[] { 2, 1 }, new int[] { 3, 4 });
		assertEquals(i, 10);
		int j = Rearrangement.dotWith(new int[] { 2, 1 }, new int[] { 3, 4, 1 });
		assertEquals(j, 0);
	}

	@Test
	public void testRotate() {
		int[] b = new int[] { 1, 2, 3 };
		Rearrangement.rotate(b);
		assertArrayEquals(new int[] {3, 1, 2}, b);
	}

	@Test
	public void testUseSorted() {
		int i = Rearrangement.useSorted(new int[] { 2, 1 }, new int[] { 3, 4 });
		assertEquals(11, i);
	}

	@Test
	public void testUseRotations() {
		int i = Rearrangement.useRotations(new int[] { 2, 1 },
				new int[] { 3, 4 });
		assertEquals(11, i);
		
		int j = Rearrangement.useRotations(new int[] { 2, 1, 3},
				new int[] { 3, 4 });
		assertEquals(0, j);
	}

	@Test
	public void testMain() {
		Rearrangement.main(new String[] { "2", "2", "1", "3", "4" });
	}

}