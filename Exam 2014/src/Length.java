
public class Length {
	private int number;
	private String units;
	
	Length() {
		this.number = 0;
		this.units = "km";
	}
	
	private boolean validUnits(String units) {
		if (units.equals("km") ||
			units.equals("miles") ||
			units.equals("minutes")) {
				return true;
		}
		return false;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		if (number >= 0)
			this.number = number;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		if (validUnits(units))
			this.units = units;
	}
	
	public void convert(String newUnits, double rate) {
		if (validUnits(newUnits)) {
			setNumber((int) Math.round(number/rate));
			setUnits(newUnits);
		}
	}
	
	
	public String toString() {
		return number + " " + units;
	}
}
