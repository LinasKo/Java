import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class GymSessionTest {
	
	GymSession g;

	@Before
	public void setUp()  {
		g = new GymSession("Gym", "The Pleasance");
	}

	@Test
	public void testToString() {
		String s = g.toString();
	}

	@Test
	public void testAddMachine() {
		// Passing null for the Length, so that if the student's Length code is all
		// wrong but their GymSession code is OK, we don't make this test
		// fail - only their Length tests should fail in that case.
		Length l = new Length();
		l.setNumber(10);
		l.setUnits("minutes");
		g.addMachine("Treadmill", l);
		g.addMachine("Cross-trainer", l);
		
		StringBuffer sb = new StringBuffer();
		sb.append("Gym (The Pleasance)\n");
		sb.append("Machines:\n");
		sb.append("Treadmill 10 minutes\n");
		sb.append("Cross-trainer 10 minutes");
		
		assertEquals(sb.toString(), g.toString());
	}

}