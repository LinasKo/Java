import java.util.Arrays;


public class Rearrangement {
	
	public static int dotWith(int[] a, int[] b){
		int sum = 0;
		if (a.length == b.length){
			for (int i = 0; i < a.length; i++) {
				sum += a[i] * b[i];
			}
		}
		return sum;
	}
	
	public static void rotate(int[] b){
		int[] tmp = new int[b.length];
		for (int j = 0; j < b.length; j++) {
			tmp[j] = b[j];
		}
		for (int i = 0; i < b.length; i++){
			b[i] = tmp[(i-1+b.length)%b.length];
		}
	}
	
	public static int useRotations(int[] a, int[] b){
		// Could be made faster by checking a==b at first.
		int maxVal = dotWith(a, b);
		for (int i = 0; i < b.length; i++){
			rotate(b);
			int val = dotWith(a, b);
			if (val > maxVal)
				maxVal = val;
			}
		return maxVal;
	}
	
	public static int useSorted(int[] a, int[] b){
		Arrays.sort(a);
		Arrays.sort(b);
		return dotWith(a,b);
	}
	
	public static void main(String[] args){
		int length = Integer.parseInt(args[0]);
		int[] a = new int[length];
		int[] b = new int[length];
		for (int i = 0; i < length; i++) {
			a[i] = Integer.parseInt(args[1+i]);
			b[i] = Integer.parseInt(args[1+length+i]);
		}
		System.out.println("dotWith gave: " + dotWith(a, b));
		System.out.println("useRotations gave: " + useRotations(a, b));
		System.out.println("useSorted gave: " + useSorted(a, b));
	}
}
