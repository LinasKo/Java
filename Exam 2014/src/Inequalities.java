
public class Inequalities {

	public static int dotProduct(int[] a, int[] b){
		int sum = 0;
		if (a.length == b.length) {
			for (int i = 0; i < a.length; i++){
				sum += a[i] * b[i];
			}
		}
		return sum;
	}
	
	public static int[] concatenate (int[] a, int[] b){
		int[] res = new int[a.length + b.length];
		for (int i = 0; i < a.length; i++) {
			res[i] = a[i];
		}
		for (int j = 0; j < b.length; j++) {
			res[j+a.length] = b[j];
		}
		return res;
	}
	
	public static boolean cs(int[] a, int[] b) {
		if (a.length == b. length) {
			if (Math.pow(dotProduct(a, b), 2) <= dotProduct(a, a) * dotProduct(b, b))
				return true;
		}
		return false;
	}
	
	public static boolean amgm(int[] a) {
		if (a.length == 0) 
			return false;
		double sum = 0;
		double product = 1;
		for (int i : a){
			sum += i;
			product *= i;
		}
		return sum/a.length >= Math.pow(product, 1.0/a.length);
	}
	
	public static void main(String[] args) {
		int len = Integer.parseInt(args[0]);
		int[] a = new int[len];
		int[] b = new int[len];
		for (int i = 0; i < len; i++) {
			a[i] = Integer.parseInt(args[1+i]);
			b[i] = Integer.parseInt(args[1+i+len]);
		}
		System.out.println("CS held: " + cs(a, b));
		System.out.println("AMGM held: " + amgm(concatenate(a, b)));
	}

}
