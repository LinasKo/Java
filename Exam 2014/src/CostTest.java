import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class CostTest {

	Cost c;
	
	@Before
	public void setUp() {
		c = new Cost();
	}

	@Test
	public void testGetCurrency() {
		String s = c.getCurrency();
	}

	@Test
	public void testSetCurrency() {
		c.setCurrency("pounds sterling");
	}

	@Test
	public void testGetAmount() {
		int i = c.getAmount();
	}

	@Test
	public void testSetAmount() {
		c.setAmount(1);
	}

	@Test
	public void testToString() {
		String s = c.toString();
	}

	@Test
	public void testConvert() {
		c.convert("pounds sterling", 1.0);
	}
	
	@Test
	public void testRounding() {
		c.setAmount(5);
		c.convert("euros", 2);
		assertEquals(3, c.getAmount());
		
		c.setAmount(5);
		c.convert("euros", 4);
		assertEquals(1, c.getAmount());
		
		c.setAmount(5);
		c.convert("euros", 3);
		assertEquals(2, c.getAmount());
	}

}