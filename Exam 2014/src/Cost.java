
public class Cost {
	private int amount;
	private String currency;

	Cost(){
		this.amount = 0;
		this.currency = "pounds sterling";
	}
	
	private boolean isAcceptable(String currency){
		// Could also be made using arrays to store calid currencies
		if (currency.equals("pounds sterling") ||
			currency.equals("US dollars") ||
			currency.equals("euros")){
				return true;
		}
		return false;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		if (amount >= 0)
			this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		if (isAcceptable(currency))
			this.currency = currency;
	}
	
	public void convert(String newCurrency, double rate){
		if (isAcceptable(newCurrency)){
			setCurrency(newCurrency);
			if (rate > 0) {
				setAmount((int)Math.round(amount/rate));
			}
		}
	}
	
	public String toString(){
		return amount + " " + currency;
	}

}
