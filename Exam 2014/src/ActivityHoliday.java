import java.util.HashMap;


public class ActivityHoliday extends Holiday {
	private HashMap<String, Cost> activities;
	
	public ActivityHoliday(String destination, int days) {
		super(destination, days);  // Ne iki galo suprantu, kaip veikia
		activities = new HashMap<String, Cost>(10);
	}
	
	public void addActivity(String name, Cost cost) {
		activities.put(name, cost);
	}
	
	@Override
	public String toString(){
		// Inefficient. Could be optimized using StringBuffer
		String s = super.toString() + "\nActivities:";
		for (String key : activities.keySet()) {
			s += String.format("\n%s %s", key, activities.get(key).toString());
		}
		return s;
	}
}
