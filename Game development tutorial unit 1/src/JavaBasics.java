public class JavaBasics {

	public static void main(String[] args) {

		final String[] words = new String[]{"Peanut","Butter","Jelly","Time"};
		Thread t = new Thread() {
			public void run() {
				for(String word : words) {
					System.out.println(word);
				}
				System.out.println("--- --- ---");
			}
		};

		Thread threadTwo = new Thread() {
            public void run() {
                for (int i = 0; i < 10; i += 2) {
                    System.out.println("hello this is thread two");
                }
            }
        };
		
		t.start();
		
		threadTwo.start();
	}

}
