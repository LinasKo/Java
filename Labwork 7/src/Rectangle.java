
public class Rectangle {

	private Point2DDouble topLeft, bottomRight;
	
	public Rectangle(Point2DDouble topLeft, Point2DDouble bottomRight) {
		this.topLeft = topLeft;
		this.bottomRight = bottomRight;
	}
	
	public Rectangle() {
		this.topLeft = new Point2DDouble(0.0, 0.0);
		this.bottomRight = new Point2DDouble(1.0, 1.0);
	}
	
	public boolean isPointInside(Point2DDouble pt) {
		if (pt.getX() >= this.topLeft.getX() && pt.getX() <= this.bottomRight.getX()) {
			if (pt.getY() >= this.topLeft.getY() && pt.getY() <= this.bottomRight.getY()) {
				return true;
			}	
		}
		return false;
	}
}
