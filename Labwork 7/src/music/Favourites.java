package music;

import java.util.ArrayList;

public class Favourites {
	
	private ArrayList<MusicTrack> favourites = new ArrayList<MusicTrack>();
	
	//this statement
	public void addTrack(String artist, String title) {
		MusicTrack mt = new MusicTrack(artist, title);
		this.favourites.add(mt);
	}
	
	public void showFavourites() {
		for (MusicTrack mt : this.favourites) {
			System.out.print(mt.toString() + "\n");
		}
	}
	
	public static void main(String[] args) {
		Favourites favourites = new Favourites();
		favourites.addTrack("Fun", "Some Nights");
		favourites.addTrack("Oliver Tank", "Help You Breathe");
		favourites.addTrack("Horse Feathers", "Fit Against the Country");
		favourites.addTrack("Emile Sande", "Country House");
		favourites.addTrack("Fun", "Walking the Dog");
		favourites.addTrack("Porcelain Raft", "Put Me To Sleep");
		favourites.showFavourites();
	}
}
