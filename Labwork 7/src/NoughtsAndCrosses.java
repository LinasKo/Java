
public class NoughtsAndCrosses {

	private int[][] board;
	
	public NoughtsAndCrosses(int[][] board) {
		this.board = board;
	}
	
	public boolean isDraw() {
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				if (this.board[row][col] != 0) {
					if (this.board[row][col] == this.board[(row+1)%3][col] && this.board[row][col] == this.board[(row+2)%3][col]) {
						return false;
					}
					if (this.board[row][col] == this.board[row][(col+1)%3] && this.board[row][col] == this.board[row][(col+2)%3]) {
						return false;
					}
				}
			}
		}
		if (this.board[0][0] == this.board[1][1] && this.board[0][0] == this.board[2][2] && this.board[0][0] != 0) {
			return false;
		}
		if (this.board[0][2] == this.board[1][1] && this.board[0][2] == this.board[2][0] && this.board[0][2] != 0) {
			return false;
		}
		return true;
	}
	
	public int whoWon() {
		if (!isDraw()) {
			for (int row = 0; row < 3; row++) {
				for (int col = 0; col < 3; col++) {
					if (this.board[row][col] != 0) {
						if (this.board[row][col] == this.board[(row+1)%3][col] && this.board[row][col] == this.board[(row+2)%3][col]) {
							return this.board[row][col];
						}
						if (this.board[row][col] == this.board[row][(col+1)%3] && this.board[row][col] == this.board[row][(col+2)%3]) {
							return this.board[row][col];
						}
					}
				}
			}
			if (this.board[0][0] == this.board[1][1] && this.board[0][0] == this.board[2][2] && this.board[0][0] != 0) {
				return this.board[1][1];
			}
			if (this.board[0][2] == this.board[1][1] && this.board[0][2] == this.board[2][0] && this.board[0][2] != 0) {
				return this.board[1][1];
			}
		}
		return 0;
	}
	
	public static void main(String[] args) {
		
	}
}
