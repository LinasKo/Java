
public class Circle {

	private Point2DDouble centre;
	private  double radius;
	
	public Circle(Point2DDouble centre, double radius) {
		this.centre = centre;
		this.radius = radius;
	}
	
	public Circle() {
		this.centre = new Point2DDouble(0.0, 0.0);
		this.radius = 1.0;
	}
	
	public boolean isPointInside(Point2DDouble pt) {
		if (Point2DDouble.distance(this.centre, pt) < radius) {
			return true;
		}
		return false;
	}
	
	/*
	public Circle(Point2DDouble centre, double radius)
	Class constructor.
	public Circle()
	Class constructor. The centre is a point at (0, 0) and the radius is 1.
	public boolean isPointInside(Point2DDouble pt)
	Returns true if the circle contains the point pt. You can compute this by calculating the distance between the pt and the circle�s centre, and seeing if this is smaller than the radius.
	*/
}
