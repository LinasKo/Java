
public class Interval {

	private double left, right;
	
	public Interval(double left, double right) {
		this.left = left;
		this.right = right;
	}
	
	public boolean doesContain(double x) {
		if (!this.isEmpty()) {
			if (x <= this.right && x >= this.left) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isEmpty() {
		if (this.left > this.right) {
			return true;
		}
		return false;
	}
	
	public boolean intersects(Interval b) {
		if (!this.isEmpty() && !b.isEmpty()) {
			if (b.doesContain(this.left) || b.doesContain(this.right)) {
				return true;
			}
		}
		return false;
	}
	
	public String toString() {
		if (this.isEmpty()) {
			return "Interval: (EMPTY)";
		}
		else {
			return "Interval: [" + this.left + ", " + this.right + "]";
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
