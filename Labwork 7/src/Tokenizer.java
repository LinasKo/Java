
public class Tokenizer {
	
	private String[] tokenizer;
	
	public Tokenizer() {
		this.tokenizer = new String[]{};
	}
	
	public Tokenizer(String fname) {
		this.tokensFromFile(fname);
	}
	
	public void tokensFromFile(String fname) {
		In file = new In(fname);
		String s = file.readAll();
		tokenize(s);
	}
	
	public void tokenize(String str) {
		this.tokenizer = str.split("\\W+");
	}
	
	public String[] getTokens() {
		return this.tokenizer;
	}
	
	public int getNumberTokens() {
		return tokenizer.length;
	}
	
	public static void main(String[] args) {
		//Use the constructor to read in a file
		Tokenizer t0 = new Tokenizer("melville-moby_dick.txt");
		String[] tokens0 = t0.getTokens();

		//Call tokensFromFile() to read in a file
		Tokenizer t1 = new Tokenizer();
		t1.tokensFromFile("melville-moby_dick.txt");
		String[] tokens1 = t1.getTokens();

		//Call tokenize() on a string
		Tokenizer t2 = new Tokenizer();
		String sent = "Together we can change the world.";
		t2.tokenize(sent);
		String[] tokens2 = t2.getTokens();
	}
}
