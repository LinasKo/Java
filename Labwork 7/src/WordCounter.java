
import static org.junit.Assert.assertArrayEquals;

import java.util.HashMap;

public class WordCounter {

	private HashMap<Integer, Integer> hash;
	
	public WordCounter(String[] tokens) {
		this.wordLengthFreq(tokens);
	}
	
	public void wordLengthFreq(String[] tokens) {
		hash = new HashMap<Integer, Integer>();
		for (String t : tokens) {
			if(this.hash.get(t.length()) == null) {
				this.hash.put(t.length(), 1);
			}
			else {
				this.hash.put(t.length(), this.hash.get(t.length()) + 1);
			}
		}
	}
	
	public HashMap<Integer, Integer> getFreqDist() {
		return this.hash;
	}
	
	public int maxVal() {
		int m = 0;
		for (Integer key : this.hash.keySet()) {
			if (hash.get(key) > m) {
				m = hash.get(key);
			}
		}
		return m;
	}
	
	public int maxKey(){
		int m = 0;
		for (Integer key : this.hash.keySet()) {
			if (key > m) {
				m = key;
			}
		}
		return m; 
	}
	
	public double[] map2array() {
		if (this.maxVal() == 0) {
			return new double[]{};
		}
		int mv = this.maxVal();
		int mk = this.maxKey();
		double[] res = new double[mk+1];
		for (Integer key : this.hash.keySet()) {
			res[key] = (double) this.hash.get(key) * 100 / mv;
		}
		return res;
	}
	
	public static void main(String[] args) {
	
		Tokenizer tokenizer = new Tokenizer("melville-moby_dick.txt");
		String[] tokens = tokenizer.getTokens();

		WordCounter wordCounter = new WordCounter(tokens);
		System.out.println(wordCounter.getFreqDist());
		double[] points = wordCounter.map2array();

		int n = points.length;
		StdDraw.clear();
		StdDraw.setXscale(0, n - 1);
		StdDraw.setYscale(0, 100);
		StdDraw.setPenRadius(0.5 / n);
		for (int i = 0; i < n; i++) {
		    StdDraw.line(i, 0, i, points[i]);
		}
	}
}
