//Imports are listed in full to show what's being used
//could just import javax.swing.* and java.awt.* etc..
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GuiApp1 {

	public static final int DEFAULT_THREAD_WIDTH = 4;
	public static final String DEFAULT_COLOR = "#000000";

	public static int threadWidth = DEFAULT_THREAD_WIDTH;

	// Note: Typically the main method will be in a
	// separate class. As this is a simple one class
	// example it's all in the one class.
	public static void main(String[] args) {
		new GuiApp1();
	}

	public void chooseColor(JFrame jframe) {
		Color selectedColor = JColorChooser.showDialog(jframe, "Pick a Color", Color.GREEN);
	}

	public GuiApp1() {

		// //// Main Frame and JPanel creation
		final JFrame guiFrame = new JFrame();

		guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		guiFrame.setTitle("Example GUI");
		guiFrame.setSize(900, 600);

		guiFrame.setLocationRelativeTo(null);

		JPanel leftStack = new JPanel();
		leftStack.setLayout(new BoxLayout(leftStack, BoxLayout.PAGE_AXIS));

		// //// Thread width JPanel
		JPanel threadPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel threadLbl = new JLabel("Thread Width");
		JTextField threadField = new JTextField(new Integer(DEFAULT_THREAD_WIDTH).toString(), 5);
		JLabel threadUnitsLbl = new JLabel("px");

		threadPanel.add(threadLbl);
		threadPanel.add(threadField);
		threadPanel.add(threadUnitsLbl);

		leftStack.add(threadPanel);

		// //// Color Panel;

		// Heading
		JPanel colorHPanel = new JPanel();
		JLabel colorsHeading = new JLabel("Colors:");
		colorHPanel.add(colorsHeading);

		leftStack.add(colorHPanel);

		// Colors
		if (ColorPanels.getColors().size() == 0)
			ColorPanels.addColor();

		for (JPanel color : ColorPanels.getColors()) {
			leftStack.add(color);
		}

		// extra button
		JPanel extraButPan = new JPanel();
		ImageIcon plus = new ImageIcon("plus.png");
		JButton extraBut = new JButton(plus);
		extraButPan.add(extraBut);

		leftStack.add(extraButPan);

		extraBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				ColorPanels.addColor();
			}
		});

		// //// Button
		JButton changesBut = new JButton("Apply Changes");

		/*
		 * The ActionListener class is used to handle the event that happens
		 * when the user clicks the button. As there is not a lot that needs to
		 * happen we can define an anonymous inner class to make the code
		 * simpler.
		 */
		changesBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				// redraw
				System.out.println(ColorPanels.getColors().size());
				new GuiApp1();
				//guiFrame.revalidate();
				//guiFrame.repaint();
			}
		});

		/*
		 * The JFrame uses the BorderLayout layout manager. Put the JPanels and
		 * JButton in different areas.
		 */

		guiFrame.add(leftStack, BorderLayout.WEST);
		guiFrame.add(changesBut, BorderLayout.SOUTH);

		guiFrame.setVisible(true);
	}

}