import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

public class DrawRectPanel extends JPanel {

	private static final long serialVersionUID = -7881645506368865475L;

	boolean swoosh;

	public void setSwoosh(boolean trueFalse) {
		swoosh = trueFalse;
	}

	public boolean getSwoosh() {
		return swoosh;
	}

	DrawRectPanel() {
		Dimension d = new Dimension(30, 30);
		this.setPreferredSize(d);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (swoosh) {
			g.setColor(Color.BLACK);
		} else {
			g.setColor(Color.GREEN);
		}
		g.fillRect(5, 5, 20, 20);
	}
}

class ColorListener implements ActionListener {
	
	// TODO unfinished

	@Override
	public void actionPerformed(ActionEvent e) {
		
		// TODO Auto-generated method stub
		
	}
	
	
}
