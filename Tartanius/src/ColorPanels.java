import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JTextField;


public class ColorPanels {

	private static ArrayList<JPanel> colors = new ArrayList<JPanel>();
	
	public static void addColor() {
		JPanel colorRow = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField colorText = new JTextField("#000000", 7);

		colorRow.add(colorText);
		DrawRectPanel rect1 = new DrawRectPanel();
		colorRow.add(rect1);

		colors.add(colorRow);
	}
	
	
	/*
	 * button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				ColorPanels.addColor();
			}
		});
	 */
	public static ArrayList<JPanel> getColors() {
		return colors;
	}
}
