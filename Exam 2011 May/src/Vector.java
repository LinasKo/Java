
public class Vector {
	private int xdisp, ydisp;
	
	public Vector(int xdisp, int ydisp) {
		this.xdisp = xdisp;
		this.ydisp = ydisp;
	}

	public int getXdisp() {
		return xdisp;
	}

	public int getYdisp() {
		return ydisp;
	}
		
	public double magnitude() {
		return Math.sqrt(xdisp*xdisp + ydisp*ydisp);
	}
	
	public Point translate(Point p) {
		return new Point(xdisp + p.getX(), ydisp + p.getY());
	}
	
}
