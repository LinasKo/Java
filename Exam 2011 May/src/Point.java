
public class Point {
	private int x, y;
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Point() {
		this.x = 0;
		this.y = 0;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public Point makeMax(Point p) {
		return new Point(Math.max(x, p.getX()), Math.max(y, p.getY()));
	}
	
	public Point makeMin(Point p) {
		return new Point(Math.min(x, p.getX()), Math.min(y, p.getY()));
	}
	
	
}
