public class OneB {

	public static double[] movingAvg(double[] data, int len) {
		
		double[] res = new double[data.length - len + 1];
		for (int i = 0; i <= data.length - len; i++) {
			double sum = 0;
			for (int j = 0; j < len; j++) {
				sum += data[i+j];
			}
			res[i] = sum/len;
		}
		return res;
	}
}
