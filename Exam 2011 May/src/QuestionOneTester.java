public class QuestionOneTester {

	public static void main(String[] args) {
		
		System.out.println(OneA.harmonicMean(new int[] {1, 2, 3}));
		System.out.println(OneA.harmonicMean(new int[] {1, 2, 4}));
		System.out.println(OneA.harmonicMean(new int[] {3, 5, 7, 9}));
		System.out.println();
		
		double[] ma1 = OneB.movingAvg(new double[] {3, 3, 6, 2, 8, 9}, 3);
		double[] ma2 = OneB.movingAvg(new double[] {22.27, 22.19, 22.08, 22.17, 22.18,
				22.13, 22.23, 22.43}, 5);
		
		for(double d : ma1) {
			System.out.print(d + ",");
		}
		System.out.println();
		
		for(double d : ma2) {
			System.out.print(d + ",");
		}
		System.out.println();
		System.out.println();
		
		for(String d : OneC.getDomainLabels("www.GROUPON.co.uk/Edinburgh+Coupons")) {
			System.out.print(d + ",");
		}
		System.out.println();
		String[] labels = {"www", "GROUPON", "co", "uk"};
		OneC.reverseArray(labels);
		for(String d : labels) {
			System.out.print(d + ",");
		}
		System.out.println();
		System.out.println();
		System.out.println(OneC.arrayToString(new String[] {"com", "TablaoCordobes", "www"}));
		
	}
}
