
public class Polygon {
	private Vector[] vectors;
	private Point[] pointList;
	
	public Polygon(Vector[] vectors) {
		this.vectors = vectors;
		
		pointList = new Point[vectors.length + 1];
		Point p = new Point();
		pointList[0] = p;
		for (int v = 1; v < vectors.length+1; v ++) {
			p = vectors[v].translate(p);
			pointList[v] = p;
		}		
	}
	
	public Vector[] getVectors() {
		return vectors;
	}
	
	public double parameter() {
		double sum = 0;
		for (Vector v : vectors) {
			sum += v.magnitude();
		}
		return sum;
	}
	
	public boolean isClosed() {
		Point p = new Point();
		for (Vector v : vectors) {
			p = v.translate(p);
		}
		if (p == new Point()) return true;
		else return false;
	}
	
	public double area() {
		if (!isClosed()) return 0.0;
		double ar = 0;
		for (int i = 0; i < vectors.length; i++) {
			double b1 = (pointList[i].getX() * pointList[i+1].getY());
			double b2 = (pointList[i+1].getX() * pointList[i].getY());
			//double a1 = (vectors[i].getXdisp() * vectors[(i+1)%(vectors.length)].getYdisp());
			//double a2 = (vectors[(i+1)%vectors.length].getXdisp()*vectors[i].getYdisp());
			ar += b1 - b2;
		}
		return Math.abs(0.5*ar);
	}
	
	public Point[] boundingBox() {
		Point pmin = pointList[0];
		Point pmax = pointList[0];
		for (Point p : pointList) {
			pmin = p.makeMin(pmin);
			pmax = p.makeMax(pmax);
		}
		return new Point[]{pmin, pmax};
	}
}
