public class OneA {

	public static double harmonicMean(int[] nums) {
		double sum = 0;
		for (int n : nums) {
			sum += (double) 1/n;
		}
		return nums.length / sum;
	}
}
