import java.util.ArrayList;

public class OneC {

	public static String[] getDomainLabels(String url) {
		for (int i = 0; i < url.length(); i++) {
			if (url.charAt(i) == '/') {
				url = url.substring(0, i);
				break;
			}
		}
		return url.split("\\.");
	}

	public static void reverseArray(String[] labels) {
		String [] tmp = new String[labels.length];
		for (int i = 0; i < labels.length; i++) {
			tmp[i] = labels[labels.length-i-1];
		}
		
		for(int i = 0; i < labels.length; i++) {
			labels[i] = tmp[i];
		}
	}

	public static String arrayToString(String[] labels) {
		if (labels.length == 0) return "";
		String s = labels[0];
		for (int i = 1; i < labels.length; i++) {
			s += ".";
			s += labels[i];
		}
		return s;
	}

	/*
	public static ArrayList<String> textToReversedDomains(String filename) {
		StdIn.redirectInput(filename);
	} */

}