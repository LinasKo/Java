
public class SplitPathName {

	public static String[] splitPath(String s) {
		String[] res = new String[4];
		int lio = s.lastIndexOf('/');
		if (lio == -1) {
			res[0] = "";
			res[1] = s;
			if (s.indexOf('.') != -1) {
				res[2] = s.substring(0,s.indexOf('.'));
				res[3] = s.substring(s.indexOf('.'), s.length());
			}
			else {
				res[2] = s;
				res[3] = "";
			}
		}
		else {
			res[0] = s.substring(0, lio+1);
			res[1] = s.substring(lio+1, s.length());
			if (res[1].indexOf('.') != -1) {
				res[2] = res[1].substring(0,res[1].indexOf('.'));
				res[3] = res[1].substring(res[1].indexOf('.'), res[1].length());
			}
			else {
				res[2] = res[1];
				res[3] = "";
			}
		}
		return res;
	}
	
	public static void main(String[] args) {
		for (int file = 0; file < args.length; file++) {
			String[] res = splitPath(args[file]);
			System.out.print("File: " + res[1] + " ");
			System.out.print("Type: " + res[3] + " ");
			System.out.print("[" + res[0] + "]\n");
		}
	}

}
