public class OneTimePadEncipher {

    public static int charToInt(char l) {
    	int i = (int) l;
    	if (i < (int)('a')) {
        	return i - (int)('A');
    	}
    	else {
        	return i - (int)('a');
    	}
    }

    public static char intToChar(int i) {
    	return ((char) (i + (int)('a')));
    }

    public static boolean isAlpha(char c) {
    	return Character.isLetter(c);
    	}

    public static String encipher(String original, String onetimepad) {
    	String res = "";
    	for (int i = 0; i < original.length(); i++) {
    		if (original.charAt(i) == ' ') {
    			res += " ";
    		}
    		else {
    			int orig = charToInt(original.charAt(i));
    			int key = charToInt(onetimepad.charAt(i));
    			res += intToChar((orig + key) % 26);
    		}
    	}
    	return res;
    }

    
    public static void main(String[] args) {
      String ciphertext = encipher("HELLO EVERYBODY", "MYSECRETKETMYSECRETKEY");
      System.out.print(ciphertext);
    }

}