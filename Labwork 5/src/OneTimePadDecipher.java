
public class OneTimePadDecipher {
	
	public static String decipher(String encipheredText, String onetimepad) {
		String res = "";
    	for (int i = 0; i < encipheredText.length(); i++) {
    		if (encipheredText.charAt(i) == ' ') {
    			res += " ";
    		}
    		else {
    			int orig = OneTimePadEncipher.charToInt(encipheredText.charAt(i));
    			int key = OneTimePadEncipher.charToInt(onetimepad.charAt(i));
    			res += OneTimePadEncipher.intToChar((orig - key + 26) % 26);
    		}
    	}
    	return res.toUpperCase();
    }
	
	public static void main(String[] args) {
		 String ciphertext = decipher("uvufsghktdal", "WHATSTHEPASSWORD");
		 System.out.print(ciphertext);
	}
}
