
import java.awt.Color;
//import java.awt.*;

public class Voronoi {
	
	public static double pointDist(Point2D point1, Point2D point2) {
		double x = point1.getX() - point2.getX();
		double y = point1.getY() - point2.getY();
		return Math.sqrt(x*x + y*y);
	}
	
	public static int findClosestPoint(Point2D point, Point2D[] sites) {
        double dist = 9999999;
        int index = -1;
        for (int i = 0; i < sites.length; i++) {
        	if (pointDist(sites[i], point) < dist) {
        		index = i;
        		dist = pointDist(sites[i], point);
        	}
        }
        return index;
    }

    public static int[][] buildVoronoiMap(Point2D[] sites, int width, int height) {
        int[][] indexmap = new int[width][height];
        for (int y = 0; y < height; y++) {
        	for (int x = 0; x < width; x++) {
        		Point2D tmp = new Point2D(x,y);
        		indexmap[x][y] = findClosestPoint(tmp, sites);
        	}
        }
        return indexmap;
    }

    public static void plotVoronoiMap(Point2D[] sites, Color[] colors, int[][] indexMap) {
    	int width = indexMap.length;
    	int height = indexMap[0].length;

    	StdDraw.setCanvasSize(width, height);
    	StdDraw.setXscale(0, width);
    	StdDraw.setYscale(0, height);
    	
    	for (int y = 0; y < height; y++) {
        	for (int x = 0; x < width; x++) {
        		StdDraw.setPenColor(colors[indexMap[y][x]]);
        		StdDraw.point(y,x);
        	}
    	}
    	StdDraw.setPenColor(Color.BLACK);
    	for (int i = 0; i < sites.length; i++) {
    		double x = sites[i].getX();
    		double y = sites[i].getY();
    		StdDraw.filledCircle(x,y,3.0);
    	}
    }

    public static void main(String[] args) {
        int width = 200;
        int height = 200;

        int nSites = 5;
        Point2D[] sites = new Point2D[nSites];
        sites[0] = new Point2D(50, 50);
        sites[1] = new Point2D(100, 50);
        sites[2] = new Point2D(50, 100);
        sites[3] = new Point2D(125, 50);
        sites[4] = new Point2D(100, 175);

        Color[] colors = new Color[nSites];
        colors[0] = Color.BLUE;
        colors[1] = Color.GREEN;
        colors[2] = Color.YELLOW;
        colors[3] = Color.ORANGE;
        colors[4] = Color.MAGENTA;

        int[][] indexmap = buildVoronoiMap(sites, width, height);
        plotVoronoiMap(sites, colors, indexmap);

    }

}