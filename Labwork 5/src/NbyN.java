
public class NbyN {

	public static int[][] nbyn(int N) {
		int[][] res = new int[N][N];
		for (int row = 0; row < N; row++) {
			for (int col = 0; col < N; col++) {
				if (row == col) res[row][col] = row;
				else res[row][col] = 0;
			}
		}
		return res;
	}
	
	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		int[][] res = nbyn(N);
		for (int row = 0; row < N; row++) {
			for (int col = 0; col < N; col++) {
				System.out.print(res[row][col]);
				if (col != N-1) System.out.print(" ");
			}
			System.out.print("\n");
		}
	}
}
