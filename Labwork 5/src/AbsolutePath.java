
public class AbsolutePath {
	
	public static String ensureAbsolute(String path) {
		if (path.charAt(0) == '/') return path;
		else return System.getProperty("user.dir") + '/' + path;
	}
	
	public static String[] absoluteSplitPath(String s) {
		return SplitPathName.splitPath(ensureAbsolute(s));
	}
	
	public static void main(String[] args) {
		for (int j = 0; j < args.length; j++) {
			String[] res = absoluteSplitPath(ensureAbsolute(args[j]));
			for (int i = 0; i < 4; i++) {
				System.out.println(res[i]);
			}
		}
	}
}
