import java.util.ArrayList;


public class Euler69 {

	public static void main(String[] args) {
		
		// Initialize
		final int MAX_NUM = 1000000;
		int root = (int) Math.sqrt(MAX_NUM);
		int[] primes_tmp = new int[root+1];
		for (int i = 0; i <= root; i++) {
			primes_tmp[i] = i;
		}
		for (int i = 2; i <= root; i++) {
			for (int j = 2; i*j <= root; j++) {
				primes_tmp[i*j] = 0;
			}
		}
		ArrayList<Integer> primes = new ArrayList<>();
		for (int p : primes_tmp) { // 1 is technically not prime, but meh :)
			if (p != 0) {
				primes.add(p);
			}
		}
		
		// Do Magic
		int product = 1;
		int index = 0;
		if (primes.isEmpty()) {
			System.out.println("ERROR: no prime numbers found.");
		}
		else {
			while (product <= MAX_NUM && index < primes.size()) {
				product *= primes.get(index++);
			}
		}
		product /= primes.get(index-1);
		System.out.printf("Answer: %s", product);
	}
}
