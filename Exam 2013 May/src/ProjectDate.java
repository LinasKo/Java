
public class ProjectDate {
	private int weekNo, dayNo;
	
	ProjectDate(){
		weekNo = 0;
		dayNo = 1;
	}

	public int getWeekNo() {
		return weekNo;
	}

	public void setWeekNo(int weekNo) {
		this.weekNo = weekNo;
	}

	public int getDayNo() {
		return dayNo;
	}

	public void setDayNo(int dayNo) {
		if (dayNo >= 1 && dayNo <= 5) 
			this.dayNo = dayNo;
	}
	
	public void advance(int n) {
		if (n >= 0) {
			setDayNo((dayNo - 1 + n) % 5 + 1);
			setWeekNo(weekNo + (dayNo - 1 + n) / 5);
		}
	}
	
	public String toString() {
		return "Week: " + weekNo + " Day: " + dayNo;
	}
}
