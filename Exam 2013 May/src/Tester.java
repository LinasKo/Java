import java.util.ArrayList;


public class Tester {

	public static void main(String[] args) {
		Engineer e1 = new Engineer("Joe", 1800);
		Engineer e2 = new Engineer("Wilbur", 2140);
		Manager m = new Manager("Pfteven", 800);
		ArrayList<Engineer> team = new ArrayList<Engineer>();
		team.add(e1);
		team.add(e2);
		m.setTeam(team);
		
		System.out.println(m.toString());
		System.out.println();
		
		System.out.println(e2.toString());
		System.out.println();

		System.out.println("I'm fairly certain that ProjectDate works ^^");
		System.out.println();
		
		for (Pair p: Lens.get(new String[] {"foo", "bar", "froboz"})) {
			System.out.print(p.toString() + ",");
		}
		
		System.out.println();
		for (String s: Lens.create(new Pair[]{new Pair('f',3), new Pair('b',3), new Pair('f',6)})) {
			System.out.print(s + ",");
		}
		System.out.println();
		
		for(String s : Lens.put (new String[] {"foo", "bar", "froboz"},
				new Pair[] {new Pair('f',3), new Pair('b',3), new Pair('f',3)})) {
			System.out.print(s + ",");
		}
		System.out.println();
		
		for(String s : Lens.put(new String[] {"foo", "bar", "froboz"},
				new Pair[] {new Pair('f',3), new Pair('b',6), new Pair('f',3)})) {
			System.out.print(s + ",");
		}

	}
}
