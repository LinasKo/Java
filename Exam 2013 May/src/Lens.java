
public class Lens {

	public static Pair[] get(String[] source) {
		Pair[] pairs = new Pair[source.length];
		for (int i = 0; i < source.length; i++) {
			pairs[i] = new Pair(source[i].charAt(0), source[i].length());
		}
		return pairs;
	}
	
	public static String[] create(Pair[] view) {
		String[] strings = new String[view.length];
		for (int i = 0; i < view.length; i++) {
			String s = "" + view[i].getC();
			for (int j = 0; j < view[i].getN() - 1; j++) {
				s += "X";
			}
			strings[i] = s;
		}
		return strings;
	}
	
	public static String[] put(String[] oldSource, Pair[] newView) {
		if (oldSource.length != newView.length) {
			System.out.println("The lengths of the oldSource does not match newView");
			return null;
		}
		String[] newSource = new String[oldSource.length];
		for (int i = 0; i < oldSource.length; i++) {
			if (oldSource[i].charAt(0) != newView[i].getC()) {
				System.out.println("First characters don't match");
				// Should do something more
			}
			else {
				String s = "" + newView[i].getC();
				for (int j = 1; j < newView[i].getN(); j++) {
					if (j < oldSource[i].length())
						s += oldSource[i].charAt(j);
					else {
						s += "X";
					}
				}
				newSource[i] = s;
			}
		}
		return newSource;
	}
}
