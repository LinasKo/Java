import java.util.ArrayList;

public class Manager extends Engineer {
	
	private ArrayList<Engineer> team;
	
	public Manager(String name, int salary) {
		super(name, salary);
		team = new ArrayList<Engineer>();
	}

	public ArrayList<Engineer> getTeam() {
		return team;
	}

	public void setTeam(ArrayList<Engineer> team) {
		this.team = team;
	}
	
	@Override
	public String toString(){
		String s = getName() + "\nManages:";
		for(Engineer e : team) {
			s += "\n" + e.toString();
		}
		return s;
	}
}
