
public class InvestigateLens {
	
	public static boolean checkGetPut(String[] source, Pair[] view) {
		return Lens.get(Lens.put(source, view)) == view;
	}
	
	public static void main(String[] args) {
		boolean t1 = checkGetPut(new String[] {"foo"}, new Pair[]{new Pair('f',7)});
		
		Pair[] p2 = new Pair[]{new Pair('f',7), new Pair('b',3), new Pair('f',6)};
		boolean t2 = checkGetPut(new String[] {"foo", "bar", "froboz"}, p2);
		
		Pair[] p3 = new Pair[]{new Pair('f',2), new Pair('b',5), new Pair('f',3)};
		boolean t3 = checkGetPut(new String[] {"foo", "bar", "froboz"}, p3);
		
		System.out.print(t1 + "\n" + t2 + "\n" + t3 + "\n" +  (t1 && t2 && t3));
	}
}
