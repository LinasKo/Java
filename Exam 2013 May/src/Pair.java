
public class Pair {
	private char c;
	private int n;
	
	public Pair(char c, int n) {
		this.c = c;
		this.n = n;
	}
	
	public char getC() {
		return c;
	}
	public void setC(char c) {
		this.c = c;
	}
	public int getN() {
		return n;
	}
	public void setN(int n) {
		this.n = n;
	}
	
	public String toString(){
		return "[" + c + "," + n + "]";
	}
}
